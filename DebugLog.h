//
//  DebugLog.h


// Comment out the next line to disable global logging
#define ENABLE_TP_LOGGING     0

#ifdef DEBUG
#define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);

#define YES_OR_NO(x) (( x )? @"YES":@"NO")

#ifdef ENABLE_TP_LOGGING
#define DebugLog(s, ...) NSLog(s, ##__VA_ARGS__)
#else
#define DebugLog(s, ...)
#endif
#else
#define DebugLog(s, ...)
#define DLog(...)
#endif
