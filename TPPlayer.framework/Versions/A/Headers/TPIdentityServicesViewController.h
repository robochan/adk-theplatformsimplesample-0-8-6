//
//  TPIdentityServicesViewController.h
//  TPPlayer
//
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import <UIKit/UIKit.h>
#import "TPNetworkMonitor.h"
#import "TPVideoProtocols.h"
#import "TPVideoAsset.h"

@protocol TPIdentityServicesDelegate
- ( void )userIsAuthorizedToPlay:( BOOL )userIsAuthorized;
@optional
-  ( UINavigationController *)navigationControllerForLogin;
- ( void )userReceivedAuthorizationError;
- ( void )userReceivedAuthorizationError:( NSString *)errorDescription;
@end

@interface TPIdentityServicesViewController : UIViewController< TPMetaDataURLAdjuster,  TPReachabilityInterestedParty>

@property(nonatomic, weak)TPVideoAsset *currentlySelectedVideoAsset;
@property(nonatomic, assign)BOOL userHasAccessAuthorization;
@property(nonatomic, weak)id<TPIdentityServicesDelegate>delegate;

@property (weak, nonatomic) IBOutlet UILabel *getAccessLabel;
@property (weak, nonatomic) IBOutlet UILabel *authorizationDescriptionText;
@property (weak, nonatomic) IBOutlet UIImageView *lockImageView;
@property (weak, nonatomic) IBOutlet UIButton *continueButton;

// if you want another UIViewController to handle showAuthorizationError, set this to true
// the identityServicesViewController.delegate will receive userReceivedAuthorizationError:( NSString *)errorDescription
@property( nonatomic, assign)BOOL delegateWillHandleAuthorizationError;

- (IBAction)userSelectedAuthorizationButton:(id)sender;
- ( void )handleAccess;
- ( void )logout;

// these methods are really just exposed so the subclasses can override them, if necessary
- ( void )getAuthorization;
- ( void )configureView;
- ( void )registerForNotifications;
- ( void )showAuthorizationError:( NSString *)errorDescription;
- ( void )accessAuthorizationChanged:(BOOL)userIsNowAuthorized;
- (  void )checkAuthentication;
- ( void )getAuthentication;
- ( void )authorizationCheckComplete:( NSNotification *)notification;
- ( void )userLoggedIn:( NSNotification *)notification;
- ( void )userLoggedOut:( NSNotification *)notification;

@end
