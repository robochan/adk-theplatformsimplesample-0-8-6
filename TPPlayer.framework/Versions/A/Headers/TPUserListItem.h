//
//  TPUserListItem.h
//  TPPlayer
//
//  Copyright (c) 2014 thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)


#import <Foundation/Foundation.h>

@interface TPUserListItem : NSObject

@property(nonatomic, strong)NSString *identifier;
@property(nonatomic, strong)NSString *itemDescription;
@property(nonatomic, strong)NSString *title;
@property(nonatomic, strong)NSNumber *updated;
@property(nonatomic, strong)NSString *aboutID;

- ( id )initFromDictionary:( NSDictionary *)entries;

@end
