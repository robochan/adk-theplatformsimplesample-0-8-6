//
//  NSStringWithCMTimeSupport.h
//  
//
//  Compatibility: iOS 5.1+
//
//  SUMMARY
//  This is just little helper class to make dealing with CMTime easier
//  I expect we will add additional methods to it as well


#import <Foundation/Foundation.h>
#import <CoreMedia/CoreMedia.h>
#import <AVFoundation/AVFoundation.h>

#define CC_NSEC_PER_SEC 600

@interface NSStringWithCMTimeSupport : NSObject

+ (NSString *)timeStringFromSeconds:(int)totalSeconds;
+ ( CMTime )convertCMTimeFromTimeCode:(NSString *)timeCode;
+ ( CMTime )convertCMTimeFromTimeCodeWithFrames:(NSString *)timeCode;

@end
