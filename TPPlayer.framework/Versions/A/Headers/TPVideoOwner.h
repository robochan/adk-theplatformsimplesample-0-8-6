//
//  TPVideoOwner.h
//
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//
//  Compatibility: iOS 5.1+
//
//  SUMMARY
//  FNVideoOwner inherits from FNImagesOwner.  We have encountered no objects that own video that do not also have an associated image to manage.
//  FNVideoOwner adds a property for the video and ensures it is properly included in the NSCoding methods

#import <Foundation/Foundation.h>
#import "TPImagesOwner.h"

@interface TPVideoOwner : TPImagesOwner < NSCoding>

// NSDicitonary of FNVideoAsset objects keyed by bitrate
@property(nonatomic, strong)NSDictionary *videos;

@end
