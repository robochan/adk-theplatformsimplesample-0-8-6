//
//  TPFeedRequest.h
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//


#import <Foundation/Foundation.h>
#import "TPFeed.h"

@class TPFeedRequest;
@class TPSort;
@class TPRange;

@protocol TPFeedRequestDelegate

- ( void )feedRequest:( TPFeedRequest *)feedRequest isLoaded:( TPFeed *)feed;
- ( void )feedRequest:( TPFeedRequest *)feedRequest failedWithError:( NSError *)error;

@end

@interface TPFeedRequest : NSObject<NSCopying>

@property(nonatomic, strong)NSString *url;
@property(nonatomic, weak)id<TPFeedRequestDelegate>delegate;

@property(nonatomic, strong)NSString *category;
@property(nonatomic, strong)NSString *search;  // contains a search query
@property(nonatomic, strong)TPSort *sortingSpecification;
@property(nonatomic, strong)TPRange *range;
@property(nonatomic, strong)NSArray *fields;
@property(nonatomic, strong)NSArray *fileFields;
// contentFormSpecifier provides the arguments for byContent
// it should contain a dictionary with NSArrays as values
// there is a default provided @{@"byFormat":@[@"m3u", @"mpeg4"]}
@property(nonatomic, strong)NSMutableDictionary *contentFormSpecifier; 
@property(nonatomic, strong)NSDictionary *parameters;  
// when the feed is downloaded, it is cached locally and the local copy is used until it reaches
// the expiration time.  When it is requested, it is checked to see if it is within the expiration time
// if so, the local copy is used - otherwise, a new fetch occurs.  
// The pathName is the way we identify the local copy.  
// Requests with different parameter values should use different pathNames.
@property(nonatomic, strong)NSString *pathName;
@property(nonatomic, strong)NSString *contentItemClassName;

// the basic parameters that are used if you don't override them
@property(nonatomic, strong, readonly)NSDictionary *defaultParameters;  

- ( id)initWithURL:( NSString *)url delegate:(id<TPFeedRequestDelegate>)delegate;
// when feed is loaded feedRequest:isLoaded: will be called on the delegate
- ( void )requestFeed;  

@end
