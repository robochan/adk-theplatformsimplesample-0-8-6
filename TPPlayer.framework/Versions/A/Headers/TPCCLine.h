//
//  TPCCLine.h
//
//  Compatibility: iOS 5.1+
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//
//
//  SUMMARY
//  an TPCCLine object represents a single line within a closed caption. 
//  It is currently only a data object - it has no behavior - only properties
//
//  NOTE: This implementation is tuned to the current tt file encode FDM is
//  using. That encode has some idosyncrasies that are being explicitly
//  coded for.
//
//  DOCUMENTATION Notes (Remove when docs are complete)
//
//  DEVELOPMENT Notes
//  CG - Plan migration to new iOS 6.0+ NSAttributedString once so we are ready
//       once FBC decides to drop 5.1
//

#import <UIKit/UIKit.h>
#import <CoreMedia/CoreMedia.h>
#import "TPCCContent.h"

#define CC_NSEC_PER_SEC 600

@interface TPCCLine : NSObject

@property(nonatomic, assign)CMTime beginTime;
@property(nonatomic,  assign)CMTime endTime;
@property(nonatomic, strong)NSString *regionID;
@property(nonatomic, strong)NSString *style;
@property(nonatomic, assign)float originXPercent;
@property(nonatomic, assign)float originYPercent;
@property(nonatomic, assign)float widthPercent;
@property(nonatomic, assign)float heightPercent;
@property(nonatomic, strong)TPCCContent *theLine;
@property(nonatomic, assign)BOOL showBackground;
@property(nonatomic, strong)UIColor *backgroundColor;

@end
