//
//  TPCustomContentItemHolder.h
//  TPPlayer
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import <Foundation/Foundation.h>
#import "TPContentItem.h"

@protocol TPCustomContentItemHolder

- ( TPContentItem *)contentItem;

@end
