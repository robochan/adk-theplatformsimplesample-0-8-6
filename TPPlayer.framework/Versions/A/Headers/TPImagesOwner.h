//
//  TPImagesOwner.h
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//
//
//  Compatibility: iOS 5.1+
//
//  SUMMARY
//  TPImagesOwner is a useful superclass for objects that contain an TPSmartImage
//  This class provides methods to find an image, checking first to see if there is a copy in memory
//  then checking to see if there is a copy on disk, then finally sending a request for it if the two prior methods failed
//  The heavy-lifting is done by the goGetImageForRole method.  When the image finally becomes available it
//  the delegate's specified selector will be invoked with the image.
//   TPImagesOwner also keeps up with multiple requestors for the same image so that only one request is sent
//   but all requestors are serviced when the image is returned.

#import <UIKit/UIKit.h>

@class TPSmartImage;

@interface TPImagesOwner : NSObject<NSCoding>

// NSDictionary of FNSmartImages keyed by role
@property(nonatomic, strong )NSDictionary *images;
@property(nonatomic, retain )NSString *title;


- ( UIImage *)imageForRole:( NSString *)role;
- ( NSString *)imagePathForRole:( NSString *)role;
- ( void )setImage:( UIImage *)anImage forRole:( NSString *)role;
- ( TPSmartImage *)smartImageForRole:( NSString *)role;

- ( void )goGetImageForRole:(NSString *)role metadata:(id)metadata delegate:(id)delegate selector:(SEL)selector activityIndicator:(UIActivityIndicatorView *)activityIndicator;

@end
