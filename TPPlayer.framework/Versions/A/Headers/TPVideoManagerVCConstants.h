//
//  TPVideoManagerVCConstants.h
//  TPPlayer
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import <Foundation/Foundation.h>

// When these notification are emitted, the object is a dictionary with an NSNumber for player's playerRate and an NSValue for the player's currentTime
extern NSString *const kTPPlayerDidChangeRateNotification;
// When this notification is emitted, the object is the aboutID -- userInfo contains a dictionary with an NSNumber for the playbackTime and NSNumber for duration
extern NSString *const kTPPlayerResumeConditionNotification;

// If you aren't playing full-screen only, there is a play button that launches the video load/play process
// This replaces the old notification called TPPlayerDidPlayButtonHitNotification
extern NSString *const kTPTopLevelPlayButtonHitNotification;


// When these notifications are emitted, the object is an NSValue with the player's currentTime
// These are from the controls
extern NSString *const kTPPlayButtonHitNotification;
extern NSString *const kTPPauseButtonHitNotification;



// When these notifications are emitted, there is no object
extern NSString *const kTPClosedCaptionButtonHitNotification;
extern NSString *const kTPPlayFromBeginningButtonHitNotification;
extern NSString *const kTPResumeButtonHitNotification;
extern NSString *const kTPMoviePlayedToEndNotification;
extern NSString *const kTPPlayerMovieAndAdsDidFinishNotification;

// When this notification is emitted, the object contains the errorMessage
extern NSString *const kTPMovieFailedToLoadNotification;
// When this notification is emitted, there is no object -- if the url fails to load you will get this in addition to kTPMovieFailedToLoadNotification 
extern NSString *const kTPMovieNoContentNotification;

// When this notification is emitted, the object contains the NSValue with the player's currentTime
extern NSString *const kTPMoviePassed25PerCentNotification;
extern NSString *const kTPMoviePassed50PerCentNotification;
extern NSString *const kTPMoviePassed75PerCentNotification;

// When this notification is emitted, the object contains a dictionary with NSValues for the post-scrub time(postScrubValue) and the pre-scrub time(preScrubValue)
extern NSString *const kTPUserScrubbedNotification;

// When this notification is emitted, the object is the new array of AVTimedMetadat
extern NSString *const kTPTimedMetaDataChangedNotification;

// When this notification is emitted, the object is an NSValue with the duration
extern NSString *const kTPMovieReadyToPlayNotification;

// When this notification is emitted, the object is a TPContentItem
extern NSString *const kTPUserSelectedContentItemNotification;

// When this notification is emitted, there is no object
extern NSString *const kTPClosedCaptionsFirstShownNotification;

// TPAnalyticsCommander receives trackVideoAdStarted:adObject amd trackVideoAdEnded:adObject when these are emitted, 
//  so metrics trackers don't need to sign up for these notifications
// When these notifications are emitted, the object is a TPAdObject
extern NSString *const kTPAdStartedNotification;
extern NSString *const kTPAdEndedNotification;
// TPAnalyticsCommander receives trackVideoAdPodStarted:adPod amd trackVideoAdPodEnded:adPod when these are emitted
//  so metrics trackers don't need to sign up for these notifications
// When these notifications are emitted, the object is a TPAdPod
extern NSString *const kTPAdSlotStartedNotification;
extern NSString *const kTPAdSlotEndedNotification;
