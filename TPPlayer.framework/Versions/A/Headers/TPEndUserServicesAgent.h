//
//  TPEndUserServicesAgent.h
//  TPPlayer
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)

//

#import <UIKit/UIKit.h>
#import "TPBookmark.h"
#import "TPBookmarkManager.h"
#import "TPBookmarkConsumer.h"
#import "TPUserList.h"
#import "TPResumeTimeConsumer.h"
#import "TPConcurrencyInfo.h"

extern NSString *const kTPResumeTimeInfoWasRefreshedNotification;

@interface TPEndUserServicesAgent : NSObject


@property(nonatomic, strong, readonly)NSString *userListItemPostUrl;
@property(nonatomic, strong, readonly)NSString *userListItemDeleteUrl;
@property(nonatomic, strong, readonly)NSString *userToken;
@property(nonatomic, strong, readonly)NSString *clientID;
@property(nonatomic, assign)BOOL shouldStoreTimeValuesAsSeconds;

@property( nonatomic, strong) TPUserList *genericUserList;

+( TPEndUserServicesAgent *)sharedInstance;

//  CALL THIS WELL BEFORE YOU CALL registerUser - 
// in the appDelegate's - (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
// method is a good place
- ( void )setup;

- ( void )registerUser:( NSString *)loginID password:( NSString *)loginPassword;
- ( BOOL )resumeIsAvailable;
- ( void )contentChanged:( NSString *)aboutID requester:(id<TPBookmarkConsumer>)requester;
- ( void )movieStopped:(NSString *)aboutID playheadPosition:( NSTimeInterval )playheadPosition duration:(NSTimeInterval )duration;
- ( void )userOptedToPlayFromBeginning:( TPBookmark *)bookmark;
- ( void )bookMarkDeleted:( TPBookmarkManager *)bookmarkManager;

 // this method does not return a bookmark - only a value useful for showing proportions
// it uses the unfiltered userList which is refreshed every N minutes, where N is defined as 
// TPResumeUserListRefreshMinutes in the appSpecificInformation.plist file.  So this value will give you
// cross-device watched-time information that also reflects cross-device events prior the past N minutes.
- ( void )getTimeToStart:( NSString *)aboutID interestedParty:( id<TPResumeTimeConsumer>)interestedParty metadata:( id )metadata; 

- ( void )unlockConcurrency:( TPConcurrencyInfo *)concurrencyInfo;
- ( void )updateConcurrency:( TPConcurrencyInfo *)concurrencyInfo;

@end
