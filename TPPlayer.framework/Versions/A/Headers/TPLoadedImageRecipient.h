//
//  TPLoadedImageRecipient.h
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import <UIKit/UIKit.h>

@interface TPLoadedImageRecipient : NSObject

@property(nonatomic, weak)id imageLoadingDelegate;
@property(nonatomic, assign)SEL imageLoadSelector;
@property(nonatomic, weak)UIActivityIndicatorView *activityIndicator;

@end
