//
//  TPPlayerControls.h
//  tryAVPlayer
//
//  Copyright © 2014, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)

//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreMedia/CoreMedia.h>
#import "NSStringWithCMTimeSupport.h"
#import "TPSliderWithAdMarks.h"
#import  <QuartzCore/QuartzCore.h>
#import "TPPlayerView.h"
#import "TPVideoManagerVCConstants.h"

#define TICK_IMAGE_NAME @"tick_mark.png"

@protocol TPPlayerControlsDelegate
- ( void )userSelectedClosedCaptionToggleButton:( BOOL )shouldShowClosedCaptions;
- ( void )userSelectedFullScreenButton:( BOOL )shouldShowFullScreen;

- ( CGFloat )togglePlayPause;
- ( void )startScrubbing;
- ( void )stopScrubbing;
- ( void )scrub:( NSTimeInterval )value;

-(BOOL)playerIsPlaying;

@optional
- ( void )userSelectedAspectToggleButton;
- ( void )userSelectedDoneButton;

@end

@interface TPPlayerControls : UIViewController <TPTickMarkOverlayDataSource, UIGestureRecognizerDelegate>

@property(nonatomic, assign)NSTimeInterval duration;
@property(nonatomic, weak)id<TPPlayerControlsDelegate>delegate;
@property(nonatomic, weak)UIViewController *interestedParty;  // usually nil - only here for subclasses to use to get messages out - probably to the VC that owns TPPlayerVC

@property(nonatomic, weak )IBOutlet UISlider *playbackSlider;
@property(nonatomic, weak )IBOutlet UIButton *playPauseButton;
@property(nonatomic, weak )IBOutlet UIButton *fullscreenButton;
@property(nonatomic, weak )IBOutlet UILabel *statusLabel;
@property(nonatomic, weak )IBOutlet UILabel *playheadPositionLabel;
@property(nonatomic, weak )IBOutlet UILabel *chapterLabel;
@property (weak, nonatomic) IBOutlet UILabel *durationLabel;
@property (weak, nonatomic) IBOutlet UIImageView *topBarImageView;
@property (weak, nonatomic) IBOutlet UIImageView *bottomBarImageView;
@property (nonatomic, weak)NSString *videoTitle;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;
@property (weak, nonatomic) IBOutlet UIButton *ccButton;
@property(nonatomic, strong) IBOutlet TPSliderWithAdMarks *sliderWithAdMarks;
@property (weak, nonatomic) IBOutlet UIView *bottomBarWrapperView;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *aspectButton;
@property( weak, nonatomic)TPPlayerView *playerView;
@property(weak, nonatomic)IBOutlet UIView *airplayActiveView;
@property(weak, nonatomic)IBOutlet UILabel *airplayActiveLabel;
@property(weak, nonatomic)IBOutlet UIImageView *airplayActiveImageView;
@property(nonatomic, assign)BOOL airplayIsActive;
@property(nonatomic, strong)NSString *airplayDeviceName;

@property(nonatomic, assign)BOOL closedCaptionsAreAvailable;
@property(nonatomic, assign)BOOL playsFullScreenOnly;
@property(nonatomic, assign)BOOL controlsShouldFade;  // defaults to YES
//if you use this to show/hide controls, you might want to give the fail instructions to support another double-tap gestureRecognizer.
@property(nonatomic, strong)UITapGestureRecognizer *controlsVisibleTapRecognizer;

-(id)initWithPlayerView:(TPPlayerView *)playerView fullScreen:( BOOL )playerIsInFullScreenMode;
- (void)syncScrubberView:(NSTimeInterval)currentTime;
- ( void)setUpAdTicks:( NSArray *)midrollAdTimes;

- (void)playerTimeDidChange:(NSTimeInterval)timeInSeconds ;

- ( IBAction)playbackSliderMoved:(id)sender;
- (IBAction)playbackSliderDone:(id)sender;
- (IBAction)doneButtonSelected:(id)sender;
- ( IBAction)fullScreenButtonSelected:(id)sender;
- ( IBAction)ccButtonSelected:(id)sender;
- (IBAction)userHitAspectButton:(id)sender;
- ( IBAction )handlePlayAndPauseButton:( UIButton  *)sender;  // if you override this, you should call super.

- ( void )adSlotStarted:( NSString *)adPosition;
- ( void )adSlotEnded:( NSString *)adPosition;

- ( void )targetViewChanged:(UIView *)newView;
- ( void )syncPlayPauseButton;

- ( void )airplayStarted;
- ( void )airplayEnded;

- ( void )controlsWereHidden; // in case subviews need to take some additional action when the controls hide - no need to call super
- ( void )controlsWereMadeVisible; // in case subviews need to take some additional action when a user tap causes the controls to show - (call super)

// called when the UIApplicationDidBecomeActiveNotification is received
- ( void )appBecameActive:( NSNotification *)notification;


@end
