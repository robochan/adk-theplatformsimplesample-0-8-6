//
//  TPCCStyle.h
//
//  Compatibility: iOS 5.1+
//
//  SUMMARY
//  each TPCCStyle object contains relevant style information to be used within an TPClosedCaption object
//  an TPCCStyle object is referenced by its styleID
//  an TPClosedCaption object contains a dictionary keyed by styleID
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import <UIKit/UIKit.h>

@interface TPCCStyle : NSObject

@property(nonatomic, strong)NSString *styleID;
@property(nonatomic, strong)UIColor *styleColor;
@property(nonatomic, strong)UIColor *styleBackgroundColor;
@property(nonatomic, strong)NSString *styleFontFamily;
@property(nonatomic, strong)NSString *styleFontSize;
@property(nonatomic, strong)NSString *styleFontWeight;
@property(nonatomic, strong)NSString *styleFontStyle;

@end
