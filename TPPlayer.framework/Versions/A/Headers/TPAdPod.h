//
//  TPAdPod.h
//  TPPlayer
//
//  Copyright © 2014, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)

//

#import <Foundation/Foundation.h>
#import "TPAdObject.h"

@interface TPAdPod : NSObject

@property(nonatomic, strong)NSString *customID;
@property(nonatomic, strong)NSArray *adObjects;
@property(nonatomic, assign)TPAdType podType;

- ( instancetype )initWithID:( NSString *)podID type:(TPAdType)adType;


@end
