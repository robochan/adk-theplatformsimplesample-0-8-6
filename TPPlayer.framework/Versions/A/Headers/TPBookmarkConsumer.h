//
//  TPBookmarkConsumer.h
//  TPPlayer
//
//  Copyright (c) 2014 thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)


#import <Foundation/Foundation.h>

@class TPBookmark;


@protocol TPBookmarkConsumer

- ( void )bookmarkFetchComplete:( TPBookmark *)bookmark;

@end
