//
//  TPXMLManager.h
//  TPPlayer
//
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import <Foundation/Foundation.h>
#import "GDataXMLNode.h"
#import "NSStringWithCMTimeSupport.h"

@class TPDataHandler;

@interface TPXMLManager : NSObject


+ ( instancetype )sharedInstance;
- ( void )createObjectFromDataHandler:( TPDataHandler *)dataHandler;
@end
