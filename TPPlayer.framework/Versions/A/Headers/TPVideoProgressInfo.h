//
//  TPVideoProgressInfo.h
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import <Foundation/Foundation.h>

@interface TPVideoProgressInfo : NSObject<NSCoding>

@property(nonatomic, assign)NSTimeInterval resumeTime;

@end
