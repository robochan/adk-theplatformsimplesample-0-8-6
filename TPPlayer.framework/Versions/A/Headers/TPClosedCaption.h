//
//  TPClosedCaption.h
//
//  Compatibility: iOS 5.1+
//
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//
//  SUMMARY
//  TPClosedCaption holds all the info necessary to produce a single closed caption display 
//  It can have multiple regions, multiple styles and multiple lines.   The closeCaptionTimeCollection returns


#import <Foundation/Foundation.h>
#import "TPCCStyle.h"
#import "TPCCRegion.h"
#import "TPCCTimeInfo.h"
#import "TPCCLine.h"

@interface TPClosedCaption : NSObject

@property(nonatomic, strong)NSDictionary *layoutRegions;
@property(nonatomic, strong)NSDictionary *styles;
@property(nonatomic, strong)NSArray *closedCaptionLines;

// only called once for each TPClosedCaption -- returns an array of TPCCTimeInfo objects
// each TPCCTimeInfo object contains all the lines for a specific CMTime
- ( NSArray *)closeCaptionTimeCollection;


@end
