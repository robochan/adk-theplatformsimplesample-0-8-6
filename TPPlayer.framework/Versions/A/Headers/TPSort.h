//
//  TPSort.h
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//


#import <Foundation/Foundation.h>

@interface TPSort : NSObject<NSCopying>

@property(nonatomic, strong)NSString *fieldName;
@property(nonatomic, assign)BOOL isDescending;

@end
