//
//  TPFeed.h
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//


#import "TPContentList.h"

@interface TPFeed : TPContentList

@property(nonatomic, assign)NSUInteger itemCount;
@property(nonatomic, assign)NSUInteger totalCount;

@end
