//
//  TPPlayer.h
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import <Foundation/Foundation.h>
#import "DebugLog.h"

#import "TPPlayerVersion.h"

 // App-specific data source
#import "TPAppSpecificInformationManager.h"

// Data Management
#import "TPMasterDataManager.h"
#import "TPDataHandler.h"
#import "TPImageLoading.h"
#import "TPVideoOwner.h"
#import "TPImagesOwner.h"
#import "GDataXMLNode.h"
#import "TPXMLManager.h"
#import "TPJSONManager.h"

// Data Objects
#import "TPContentList.h"
#import "TPContentItem.h"
#import "TPSmartImage.h"
#import "TPSmilInfo.h"
#import "TPPlaylist.h"
#import "TPFeed.h"
#import "TPCategory.h"
#import "TPCredit.h"
#import "TPClip.h"
#import "TPBaseClip.h"
#import "TPFeedRequest.h"
#import "TPRange.h"
#import "TPSort.h"

// Video
#import "TPVideoProtocols.h"
#import "TPVideoAsset.h"
#import "TPPlayerViewController.h"
#import "TPResumeOverlay.h"
#import "TPMetadataView.h"
#import "TPPlayerView.h"
#import "TPPlayerConfiguration.h"
#import "TPHeartbeatObserver.h"
#import "TPVideoManagerVCConstants.h"

// IdentityServices
#import "TPIdentityServicesViewController.h"

// Ad Manager wrapper
#import "TPAdManager.h"
#import "TPAdManagerOwner.h"
#import "TPAdObject.h"
#import "TPAdPod.h"

// Custom Controls
#import "TPPlayerControls.h"
#import "TPSlider.h"
#import "TPSliderWithAdMarks.h"
#import "TPTickMarkOverlayView.h"

//  Closed captions
#import "TPClosedCaption.h"
#import "TPClosedCaptionManager.h"
#import "TPCCLine.h"
#import "TPCCContent.h"

// Intervals
#import "TPInterval.h"
#import "TPIntervalManager.h"

// Network Helpers
#import "TPNetworkMonitor.h"
#import "TPReachability.h"
#import "TPNetworkAwareViewController.h"

// Analytics
#import "TPAnalyticsCommander.h"
#import "TPMetricsTracker.h"
#import "TPAnalyticsTracking.h"
#import "TPCustomContentItemHolder.h"

// Handy categories
#import "NSStringWithCMTimeSupport.h"
#import "NSStringWithURLParsingSupport.h"

// Platform communication
#import "TPEndUserServicesAgent.h"
#import "TPBookmark.h"
#import "TPBookmarkConsumer.h"