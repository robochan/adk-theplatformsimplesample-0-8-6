//
//  TPUserList.h
//  TPPlayer
//
//  Copyright (c) 2014 thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)


#import <Foundation/Foundation.h>
@class TPUserListItem;

@interface TPUserList : NSObject

@property(nonatomic, strong)NSString *identifier;
@property(nonatomic, strong, readonly)NSMutableArray *userListItems;
@property(nonatomic, strong, readonly)NSMutableDictionary *userListItemsDictionary;

- ( id )initFromDictionary:( NSDictionary *)entries;
- ( NSValue *)timeToStartRangeValue:( NSString *)aboutID; 

- ( void )addUserListItem:( TPUserListItem *)userListItem;
- ( void )removeUserListItem:( TPUserListItem *)userListItem;
- ( void )updateUserListItem:( TPUserListItem *)userListItem;
@end
