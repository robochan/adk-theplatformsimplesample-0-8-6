//
//  TPVideoProtocols.h
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import <Foundation/Foundation.h>

@class TPBaseClip;

@protocol TPURLAdjuster;

@protocol TPURLAdjustmentRequester <NSObject>
// sent to a TPURLAdjustmentRequester by a TPURLAdjuster as notification when a url has been modified
- ( void )urlAdjuster:( id <TPURLAdjuster>)adjuster applyClip:(TPBaseClip *)clip;
@end

@protocol TPURLAdjuster <NSObject>

// pass through an unencoded string
// TPURLAdjuster will make modifications and return the results sending the urlAdjuster:completedModifications: message to requester
- ( void )rewriteURL:( TPBaseClip *)clip forRequester:( id<TPURLAdjustmentRequester>)requester;

@end

@protocol TPMetaDataURLAdjuster;

@protocol TPMetaDataURLAdjustmentRequester <NSObject>
// sent to a TPMetaDataURLAdjustmentRequester by a TPMetaDataURLAdjuster as notification when a url has been modified
- ( void )metaDataUrlAdjuster:( id <TPMetaDataURLAdjuster>)adjuster applyMetadataURL:(NSString *)stringForURL isPreview:( BOOL )isPreview;
@end

@protocol TPMetaDataURLAdjuster <NSObject>

// pass through an unencoded string
// TPMetaDataURLAdjuster will make modifications and return the results sending the metaDataUrlAdjuster:completedModifications: message to requester
- ( void )rewriteMetadataUrl:( NSString *)stringForURL forRequester:( id<TPMetaDataURLAdjustmentRequester>)requester isPreview:( BOOL )isPreview;

@end

