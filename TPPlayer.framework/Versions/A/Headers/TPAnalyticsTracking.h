//
//  TPAnalyticsTracking.h
//  TPPlayer
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import <UIKit/UIKit.h>
#import "TPAdPod.h"

@protocol TPAnalyticsTracking 

- ( void )trackPageView:( UIViewController *)reportingViewController;
- ( void )trackVideoView:( UIViewController *)reportingController stage:( NSString *)videoStage;
- ( void )trackVideoView:( UIViewController *)reportingController withTime:( NSUInteger)seconds stage:( NSString *)videoStage;
- ( void)trackLink:( UIViewController *)reportingController linkName:( NSString *)linkName forType:( NSString *)type;
- (void) trackSearch:(UIViewController *)reportingViewController searchTerm:(NSString *)searchTerm;
- (void) trackSocialShare:(UIViewController *)reportingController sharedItem:(NSString *)sharedItem;
- (void)trackVideoAdStarted:(UIViewController *)reportingController  adPosition:( NSString *)adPosition additionalInformation:( NSDictionary *)notificationDictionary;
- (void)trackVideoAdCompleted:(UIViewController *)reportingController  adPosition:( NSString *)adPosition additionalInformation:( NSDictionary *)notificationDictionary;
- ( void )trackVideoAdPodStarted:(UIViewController *)reportingController adPod:( TPAdPod *)adPod;
- ( void )trackVideoAdPodCompleted:(UIViewController *)reportingController adPod:( TPAdPod *)adPod;
- ( void )trackVideoAdStarted:(UIViewController *)reportingController adObject:( TPAdObject *)adObject;
- ( void )trackVideoAdCompleted:(UIViewController *)reportingController adObject:( TPAdObject *)adObject;
- ( void )trackLogin:( UIViewController *)reportingController stage:( NSString *)loginStage;

@end
