//
//  TPTickMarkOverlayView.h
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//
//

#import <UIKit/UIKit.h>

@interface TPTickMarkOverlayView : UIView

@property(nonatomic, assign)CGRect sliderFrame;


- ( void )makeMarks:( NSArray *)locationsToMark withImage:(UIImage *)tickImage;

//  these are called automatically, but are public in case you need them
- ( void )adjustMarkPositioning;  // called during layoutSubviews to recalculate the positions
- ( void )adjustMarkVisibility:( CGRect)thumbRect;  // disappears the mark over the thumbView

@end
