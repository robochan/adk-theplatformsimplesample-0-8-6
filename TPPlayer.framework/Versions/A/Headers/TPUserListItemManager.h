//
//  TPUserListItemManager.h
//  TPPlayer
//
//  Copyright (c) 2014 thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)


#import <Foundation/Foundation.h>
#import "TPUserList.h"
#import "TPUserListItem.h"

@interface TPUserListItemManager : NSObject
@property(nonatomic, strong)NSString *aboutID;
@property(nonatomic, strong)TPUserList *userList;
@property( nonatomic, assign)BOOL isProcessingListItem;
@property(nonatomic, strong)NSString *userToken;
@property(nonatomic, strong)NSString *userID;

- ( NSString *)createUserListItemURL:( TPUserListItem *)userListItem;
- ( void )createUserListItem:( TPUserListItem *)prototypeItem;
- ( void )userListItemCreationCompletedSuccessfully:( NSDictionary *)results;
- ( void )userListItemCreationFailed:( NSDictionary *)results;

- ( NSString *)updateUserListItemURL:( TPUserListItem *)userListItem;
- ( void )updateUserListItem:( TPUserListItem *)prototypeItem;
- ( void )userListItemUpdateCompletedSuccessfully:( NSDictionary *)results;
- ( void )userListItemUpdateFailed:( NSDictionary *)results;

- ( NSString *)deleteUserListItemURL:( TPUserListItem *)userListItem;
- ( void )deleteUserListItem:( TPUserListItem *)prototypeItem;
- ( void )userListItemDeleteCompletedSuccessfully:( NSDictionary *)results;
- ( void )userListItemDeleteFailed:( NSDictionary *)results;

@end
