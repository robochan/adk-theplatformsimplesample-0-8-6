//
//  TPCCTimeInfo.h
//
//  Compatibility: iOS 5.1+
//
//  SUMMARY
//  each TPCCTimeInfo object contains all the line( TPCCLine )objects for a specific CMTime
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import <Foundation/Foundation.h>
#import <CoreMedia/CoreMedia.h>

@interface TPCCTimeInfo : NSObject

@property(nonatomic, strong)NSArray *ccLines;
@property(nonatomic, assign)CMTime timeToShow;

@end
