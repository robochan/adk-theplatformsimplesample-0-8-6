//
//  TPPlaylist.h
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//


#import <Foundation/Foundation.h>
#import "TPBaseClip.h"
#import "TPConcurrencyInfo.h"

@interface TPPlaylist : NSObject

@property(nonatomic, strong)NSString    *contentID;     // MPX ID of the Media the playlist comes from
@property(nonatomic, strong)NSString    *playlistID;    // MPX ID of the Public URL the playlist comes from
@property(nonatomic, strong)NSString    *releasePID;    // MPX PID of the Public URL the playlist comes from
@property(nonatomic, strong)NSString    *feed;          // Title of the Feed the playlist comes from
@property(nonatomic, strong)TPBaseClip  *baseClip;      // Baseclip associated with the playlist clips
@property(nonatomic, strong) TPConcurrencyInfo *concurrencyInfo;

@end
