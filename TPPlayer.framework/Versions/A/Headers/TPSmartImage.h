//
//  TPSmartImage.h
//
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//
//  Compatibility: iOS 5.1+
//
//  SUMMARY
//  TPSmartImage is a very handy class for images that may be in memory, or on disk or may need to be requested from the web
//  The image property is not archived, and it is assumed that if the image is stored on disk, it will be retrieved when called for rather than
//  anytime an TPSmartImage is created.
//  When the image property is requested, it will return the copy in memory if there is one.
//  Otherwise, it will see if there is a copy on disk.  If so, it will check the file modification date to see if it is within 30 days (can change this number)
//  by overriding the localImageIsFresh method.
//  If there is a local version (and it is fresh), it is assigned to the image property and returned
//  otherwise, nil is returned and the object seeking an image typically asks the TPMasterDataManager to return an image from the web
//  [ [ TPAppMasterDataManager sharedInstance ] getImageForPath:imagePathForRole  metadata:metadata owner:imageToLoad  delegate:delegate selector:selector];

#import <UIKit/UIKit.h>
#import "TPImageLoading.h"

@interface TPSmartImage : NSObject< TPImageLoading >

@property(nonatomic, strong )NSString *parentID; // we keep parentID to create a unique name for the archived image

@property(nonatomic, strong )NSString *role;
@property(nonatomic, strong )NSURL *imageLocalURL;
@property(nonatomic, strong )NSString *imagePath;
@property(nonatomic, strong )UIImage *image; // this is not archived
@property(nonatomic, strong )NSString *photoKey;

// tP properties with strict nomenclature from JSON
@property(nonatomic, assign)NSInteger   filesize;
@property(nonatomic, assign)NSInteger   height;
@property(nonatomic, assign)NSInteger   width;
@property(nonatomic, strong)NSString    *url;
@property(nonatomic, strong)NSString    *format;
@property(nonatomic, strong)NSArray     *assetTypes;
@property(nonatomic, assign)BOOL        isDefault;
@property(nonatomic, strong)NSString    *contentType;

+ ( TPSmartImage *)smartImageFromString:( NSString *)url withParentId:( NSString *)parentId;
- ( id )initWithJsonDictionary:( NSDictionary *)imageDictionary withParentId:( NSString *)parentId;

- ( BOOL )localImageIsFresh;

@end
