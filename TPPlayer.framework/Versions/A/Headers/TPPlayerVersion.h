//
//  TPPlayerVersion.h
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#define TPPLAYER_VERSION @"0.8.6"
#define TPPLAYER_BUILD_NUMBER @"384" 