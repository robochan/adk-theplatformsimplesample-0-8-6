//
//  TPBookmarkManager.h
//  TPPlayer
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import <Foundation/Foundation.h>
#import "TPBookmarkConsumer.h"
#import "TPUserListItemManager.h"

@interface TPBookmarkManager : TPUserListItemManager

@property(nonatomic, assign)BOOL awaitingResponse;
@property(nonatomic, weak)id<TPBookmarkConsumer> requestor;

@property(nonatomic, strong)TPBookmark *bookmark;

- ( void )makeBookmarkFromUserList:( TPUserList *)userList;
- ( void )removeBookmark;

- ( void )movieStopped:(NSString *)aboutID playheadPosition:( NSTimeInterval )playheadPosition duration:(NSTimeInterval )duration;


@end
