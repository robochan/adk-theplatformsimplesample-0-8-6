//
//  TPPlayerConfiguration.h
//  TPPlayer
//
//  Copyright © 2014, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import <Foundation/Foundation.h>

@interface TPPlayerConfiguration : NSObject

@property(nonatomic, strong)NSString *playerControlClassName;


@end
