//
//  TPAdManagerOwner.h
//  TPPlayer
//
//  Copyright © 2014, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import <Foundation/Foundation.h>
#import "TPVideoManagerVCConstants.h"
#import "TPAdPod.h"

@protocol TPAdManagerOwner

- ( AVPlayer *)player;

- (void)playContentMovie;
- ( void )removeHeartbeatObserver:( id)heartbeatObserver;
- ( BOOL )playerIsPlaying;
- ( void )stopPlayer;
- ( void )startPlayer;

- (void)cleanup;

@optional
- ( void )slotStarted:( TPAdPod *)adPod;
- ( void )slotEnded:( TPAdPod *)adPod;

- ( void )adStarted:( TPAdObject *)adObject;
- ( void )adEnded:( TPAdObject *)adObject;

- ( void )slotStarted:( NSDictionary *)notificationDictionary adPosition:( NSString *)adPosition;
- ( void )slotEnded:( NSDictionary *)notificationDictionary adPosition:( NSString *)adPosition;

- ( void )adStarted:( NSDictionary *)notificationDictionary adPosition:( NSString *)adPosition;
- ( void )adEnded:( NSDictionary *)notificationDictionary adPosition:( NSString *)adPosition;

- ( NSTimeInterval)duration;


@end
