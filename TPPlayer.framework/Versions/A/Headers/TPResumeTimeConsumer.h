//
//  TPResumeTimeConsumer.h
//  TPPlayer
//
//

#import <Foundation/Foundation.h>

@protocol TPResumeTimeConsumer 

//  check these values before you use them directly.  The proportions should be correct but they may
//  be in seconds or milliseconds.  
- ( void )resumeTimeInfoIsAvailable:( NSDictionary *)resumeTimeInfo metadata:(id)metadata;

@end
