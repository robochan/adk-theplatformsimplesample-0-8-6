//
//  TPAnalyticsCommander.h
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import <Foundation/Foundation.h>
#import "TPAnalyticsTracking.h"

// all the methods in the protocol are not implemented to show how 
// the forwarding works
@interface TPAnalyticsCommander : NSObject<TPAnalyticsTracking>

+( TPAnalyticsCommander *)sharedInstance;

- ( void )didFinishLaunching;
- ( void )applicationWillResignActive;
- ( void )applicationDidEnterBackground;
- ( void )applicationWillEnterForeground;
- ( void )applicationDidBecomeActive;
- ( void )applicationWillTerminate;

@end
