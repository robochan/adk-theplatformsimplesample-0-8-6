//
//  TPConcurrencyInfo.h
//  TPPlayer
//
//  Copyright (c) 2014 thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import <Foundation/Foundation.h>

@protocol TPConcurrencyObserver

- ( void )receivedConcurrencyConflict;

@end

@interface TPConcurrencyInfo : NSObject

@property(nonatomic, strong)NSString *updateLockInterval;
@property(nonatomic, strong)NSString *concurrencyServiceURL;
@property(nonatomic, strong)NSString *lockID;
@property(nonatomic, strong)NSString *lockSequenceToken;
@property(nonatomic, strong)NSString *lockContent;
@property(nonatomic, weak )id< TPConcurrencyObserver>concurrencyObserver;

- ( id )initWithDictionary:(NSDictionary *)metaTagDictionary;

-(  NSString *)unlockURL;
-(  NSString *)updateURL;

@end
