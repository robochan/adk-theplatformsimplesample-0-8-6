//
//  TPCCRegion.h
//
//  Compatibility: iOS 5.1+
//
//  SUMMARY
//  Each TPCCRegion object represents a space on the screen to be populated by a single closed caption
//  It is uniquely identified by its regionID
//  It is visually represented by an TPClosedCaptionView
//
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import "TPClosedCaptionView.h"

@interface TPCCRegion : NSObject

@property(nonatomic, strong)NSString *regionID;
@property(nonatomic, strong)UIColor *regionBackgroundColor;
@property(nonatomic, strong)NSString *regionShowBackground;
@property(nonatomic, strong)TPClosedCaptionView *regionSubview;


@end
