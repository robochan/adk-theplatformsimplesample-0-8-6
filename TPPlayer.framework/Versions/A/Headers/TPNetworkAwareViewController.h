//
//  TPNetworkAwareViewController.h
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import <UIKit/UIKit.h>
#import "TPNetworkMonitor.h"


@interface TPNetworkAwareViewController :  UIViewController< TPReachabilityInterestedParty>

@property(nonatomic, assign)BOOL dataLoadIsComplete;  // subclasses each need to populate this
@property(nonatomic, assign)BOOL dataLoadingIsInProgress;

- ( void )setup;
- ( void )startLoadingData;
- ( BOOL )dataNeedsRefresh;
- (void)showAlertView;

@end
