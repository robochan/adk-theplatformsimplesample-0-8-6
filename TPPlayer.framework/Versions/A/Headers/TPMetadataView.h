//
//  TPMetadataView.h
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import <UIKit/UIKit.h>

@class TPContentItem;

@interface TPMetadataView : UIView

@property (weak, nonatomic) IBOutlet UILabel *contentTitle;
@property (weak, nonatomic) IBOutlet UILabel *availabilityLabel;
@property (weak, nonatomic) IBOutlet UILabel *runtimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *availableFromLabel;
@property (weak, nonatomic) IBOutlet UILabel *availabilityValue;
@property (weak, nonatomic) IBOutlet UILabel *runtimeValue;
@property (weak, nonatomic) IBOutlet UILabel *availableFromValue;
@property (weak, nonatomic) IBOutlet UILabel *synopsis;

+ ( TPMetadataView *)metadataView;

- ( void )populateFromContentItem:( TPContentItem *)contentItem;
- (NSString *)timeFormatted:(int)totalSeconds;

@end
