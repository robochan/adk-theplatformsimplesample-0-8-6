//
//  DevAppContentItem.m
//  ThePlatformSampleWithFramework
//


#import "DevAppContentItem.h"

@implementation DevAppContentItem

- ( id )initContentItemFromJson:( NSDictionary *)jsonObjects;
{
    if (self = [ super initContentItemFromJson:jsonObjects]) {
        //  this custom field overrides the value in the videoAsset - but TPPlayer checks the videoAsset
        self.videoAsset.needsAuthorization = self.requiresAuth;
        self.videoAsset.guid = self.guid;

        self.roleForDefaultThumbnail = @"thumbnail_image_sm";
        self.roleForDefaultPosterImage = @"poster_image";
    };
    return self;
}

// this allows us to implement a scheme for assigning roles to the images.  It is called right after the smartImages are created.
- ( void )adjustRole:( TPSmartImage *)tmpSmartImage fromJsonDictionary:( NSDictionary *)imageDictionary;
{
    if ([imageDictionary[@"plfile$width"]integerValue] == 203 ) {
        tmpSmartImage.role = @"thumbnail_image_sm";
    }
    if ([imageDictionary[@"plfile$width"]integerValue] == 406 ) {
        tmpSmartImage.role = @"thumbnail_image_sm_retina";
    }
    if ([imageDictionary[@"plfile$width"]integerValue] == 622 ) {
        tmpSmartImage.role = @"poster_image";
    }
    if ([imageDictionary[@"plfile$width"]integerValue] == 1244) {
        tmpSmartImage.role = @"poster_image_retina";
    }
}

#pragma
- ( BOOL )videoIsEpisode;
{
    return self.fullEpisode;
}
- ( NSString *)seriesForVideo;
{
    return self.series;
}
- ( NSString *)seasonForVideo;
{
    return [NSString stringWithFormat:@"%d", self.season];
}

- ( NSString *)titleForVideo;
{
    return self.title;
}
- ( NSString *)identifierForVideo;
{
    return self.identifier;
}
- ( NSString *)runtimeForVideo;
{
    return [NSString stringWithFormat:@"%d", (int)self.videoAsset.duration];
}
- ( NSString *)episodeNumberForVideo;
{
    return [NSString stringWithFormat:@"%d", self.episode ];
}
- ( NSString *)descriptionForVideo;
{
    return self.synopsis;
}

- ( id )initWithCoder:(NSCoder *)aDecoder
{
    self = [ super initWithCoder:aDecoder ];
    self.episode = [ aDecoder decodeIntegerForKey:@"episode" ];
    self.season = [ aDecoder decodeIntegerForKey:@"season" ];
    self.freewheelID = [ aDecoder decodeObjectForKey:@"freewheelID" ];
    self.adManagerId = self.freewheelID;
    self.fullEpisode = [ aDecoder decodeBoolForKey:@"fullEpisode" ];
    self.videoAsset.needsAuthorization = self.requiresAuth;
    self.videoAsset.guid = self.guid;
    self.series = [ aDecoder decodeObjectForKey:@"series" ];
    
    return self;
}

- ( void )encodeWithCoder:(NSCoder *)aCoder;
{
    [ super encodeWithCoder:aCoder ];
    [ aCoder encodeInteger:self.episode forKey:@"episode" ];
    [ aCoder encodeInteger:self.season forKey:@"season" ];
    [ aCoder encodeObject:self.freewheelID forKey:@"freewheelID" ];
    [ aCoder encodeBool:self.fullEpisode forKey:@"fullEpisode"];
    [ aCoder encodeObject:self.series forKey:@"series" ];
 }

@end
