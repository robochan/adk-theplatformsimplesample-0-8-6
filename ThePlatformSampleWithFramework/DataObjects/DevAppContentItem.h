//
//  DevAppContentItem.h
//  ThePlatformSampleWithFramework
//

#import <TPPlayer/TPPlayer.h>


@interface DevAppContentItem : TPContentItem

@property(nonatomic, assign)NSUInteger episode;
@property(nonatomic, assign)NSUInteger season;
@property(nonatomic, strong)NSString *freewheelID;
@property(nonatomic, assign)BOOL fullEpisode;
@property(nonatomic, strong)NSString *series;



@end
