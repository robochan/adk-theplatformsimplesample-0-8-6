//
//  TPAppDelegate.h
//  ThePlatformSampleWithFramework
//


#import <UIKit/UIKit.h>
#import "TPPlayer/TPPlayer.h"

@interface TPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
