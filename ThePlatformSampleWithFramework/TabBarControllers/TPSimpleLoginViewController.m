//
//  TPSimpleLoginViewController.m
//  ThePlatformSampleWithFramework
//
//  Copyright © 2014, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)

//

#import "TPSimpleLoginViewController.h"
#import <TPPlayer/TPPlayer.h>

static void *kUserTokenKeyContext = &kUserTokenKeyContext;

@interface TPSimpleLoginViewController ()

@end

@implementation TPSimpleLoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    BOOL userIsLoggedIn = [TPEndUserServicesAgent sharedInstance].userToken.length > 0;
    
    if (userIsLoggedIn) {
        [self.loginButton setTitle:@"Logout" forState: UIControlStateNormal];
    }
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)userPressedLoginButton:(id)sender {
    NSString *userName = self.userNameTextField.text;
    NSString *password = self.passwordTextField.text;
    [[ TPEndUserServicesAgent sharedInstance]registerUser:userName password:password];
    [ [ TPEndUserServicesAgent sharedInstance] addObserver:self forKeyPath:@"userToken" options:NSKeyValueObservingOptionNew context:kUserTokenKeyContext ];
}

- ( void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context;
{
    if (context == kUserTokenKeyContext ) {
        if ([ keyPath isEqualToString:@"userToken"]) {
            BOOL userTokenExists = NO;
            if (change[NSKeyValueChangeNewKey] != [ NSNull null ] ) {
                userTokenExists = YES;
            }
            if (userTokenExists) {
                [self.loginButton setTitle:@"Logout" forState: UIControlStateNormal];
            }
            else{
                [self.loginButton setTitle:@"Login" forState: UIControlStateNormal];
            }

            }
         }
    [ [ TPEndUserServicesAgent sharedInstance] removeObserver:self forKeyPath:@"userToken"];
}

- ( void )dealloc;
{
    [ [ TPEndUserServicesAgent sharedInstance] removeObserver:self forKeyPath:@"userToken"];
}
@end
