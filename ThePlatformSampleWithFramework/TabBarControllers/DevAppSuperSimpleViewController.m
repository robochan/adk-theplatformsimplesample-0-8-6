//
//  DevAppSuperSimpleViewController.m


#import "DevAppSuperSimpleViewController.h"

@interface DevAppSuperSimpleViewController ()

@property( nonatomic, strong )TPPlayerViewController *playerViewController;

@end

@implementation DevAppSuperSimpleViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.playerViewController = [ [ TPPlayerViewController alloc]initWithNibName:nil bundle:nil ];
    self.playerViewController.view.frame = self.topView.bounds;
    self.playerViewController.fadeDuration = 1.0;
    self.playerViewController.playsFullScreenOnly = YES;
    [ self addChildViewController:self.playerViewController ];
    [self.topView addSubview:self.playerViewController.view ];
    [ self.playerViewController didMoveToParentViewController:self ];
    self.playerViewController.cancelButton.hidden = YES;
    [[ NSNotificationCenter defaultCenter]addObserver:self selector:@selector(contentItemLoaded:) name:TPPlayerDidReceiveContentItemNotification object:nil ];
    DLog(@"topView is %@ playerViewController view %@", self.topView, self.playerViewController.view );
	// Do any additional setup after loading the view.
}

- ( void )viewDidLayoutSubviews;
{
    DLog(@"playerViewController view %@", self.topView);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)userSelectedLoad:(id)sender 
{
    DLog();
    UIButton *selectedButton = (UIButton *)sender;
    NSUInteger buttonTag = selectedButton.tag;
    DLog(@"selectedButton tag = %d", buttonTag);
    
    NSString *urlForMetadata = nil;
    switch (buttonTag) {
        case 10:
            urlForMetadata = @"http://link.theplatform.com/s/3JHsmB/xu9cE8KESKb4tH4aFteviKTbgNu6ZDRm?feed=HLS%20Only%20Feed";
            break;
        case 20:
            urlForMetadata = @"http://link.theplatform.com/s/3JHsmB/35Q_FR_1iOzKY2N7M_u021dkJw2Rgy_5?feed=HLS%20Only%20Feed";
            break;
        case 30:
            urlForMetadata = @"http://link.theplatform.com/s/3JHsmB/HwQqM66eciB_rl6mXRMVumfpl1ARgUmV?feed=HLS%20Only%20Feed";
            break;
        case 40:
            urlForMetadata = @"http://link.theplatform.com/s/3JHsmB/KKLLCKIMvO5GvGLI_0CEKnuJi0DiWeYv?feed=HLS%20Only%20Feed";
            break;
        default:
            break;
    }
    [self.playerViewController loadReleaseURL:urlForMetadata withAdditionalParameters:nil ];
}

- (IBAction)userSelectedPlay:(id)sender
{
    DLog();
    UIButton *selectedButton = (UIButton *)sender;
    NSUInteger buttonTag = selectedButton.tag;
    DLog(@"selectedButton tag = %d", buttonTag);
    
    NSString *url = nil;
    switch (buttonTag) {
        case 110:
            url = @"http://link.theplatform.com/s/3JHsmB/xu9cE8KESKb4tH4aFteviKTbgNu6ZDRm?feed=HLS%20Only%20Feed";
            self.titleLabel.text = @"The Adventures of Tintin";
            break;
        case 120:
            url = @"http://link.theplatform.com/s/3JHsmB/35Q_FR_1iOzKY2N7M_u021dkJw2Rgy_5?feed=HLS%20Only%20Feed";
            self.titleLabel.text = @"Wrath of the Titans";
            break;
        case 130:
            url = @"http://link.theplatform.com/s/3JHsmB/HwQqM66eciB_rl6mXRMVumfpl1ARgUmV?feed=HLS%20Only%20Feed";
            self.titleLabel.text = @"The Dark Knight Rises";
            break;
        case 140:
            url = @"http://link.theplatform.com/s/3JHsmB/KKLLCKIMvO5GvGLI_0CEKnuJi0DiWeYv?feed=HLS%20Only%20Feed";
            self.titleLabel.text = @"Mission: Impossible - Ghost Protocol";
            break;
        default:
            break;
    }
    // using playReleaseURL, there is no supporting information to load, so we need to clear out whatever was there before
    [ self.playerViewController reset ];
    [self.playerViewController playReleaseURL:url withAdditionalParameters:nil ];
}

- ( void )contentItemLoaded:( NSNotification *)notification;
{
    TPContentItem *loadedContentItem = (TPContentItem *)[notification object ];
    self.titleLabel.text = loadedContentItem.title;
}

- ( void )dealloc;
{
    [[ NSNotificationCenter defaultCenter]removeObserver:self ];
}
@end
