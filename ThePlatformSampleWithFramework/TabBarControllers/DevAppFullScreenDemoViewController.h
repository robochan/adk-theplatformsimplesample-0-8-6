//
//  DevAppFullScreenDemoViewController.h
//  ThePlatformSampleWithFramework
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//
//  This ViewController plays content full-screen, uses custom controls and is configured to play FreeWheel ads.

#import <UIKit/UIKit.h>
#import <TPPlayer/TPPlayer.h>
#import "TPCustomPlayerControls.h"

@interface DevAppFullScreenDemoViewController : TPNetworkAwareViewController<TPFeedRequestDelegate, UICollectionViewDataSource, UICollectionViewDelegate, TPPlayerFullScreenDelegate, TPCustomPlayerControlsInterestedParty>

@property (weak, nonatomic) IBOutlet UILabel *noContentLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *mainActivityIndicator;
@property (weak, nonatomic) IBOutlet UICollectionView *feedCollectionView;

@end
