//
//  DevAppSuperSimpleViewController.h

#import <UIKit/UIKit.h>
#import "TPPlayer/TPPlayer.h"

@interface DevAppSuperSimpleViewController : TPNetworkAwareViewController
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIButton *userSelectedPlay;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

- (IBAction)userSelectedLoad:(id)sender;
- (IBAction)userSelectedPlay:(id)sender;

@end
