//
//  DevAppFullScreenDemoViewController.m
//  ThePlatformSampleWithFramework
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import "DevAppFullScreenDemoViewController.h"
#import "DevAppContentItem.h"
#import "DevAppSimpleCollectionViewCell.h"
#import <TPFreeWheelAdManager/TPFreeWheelAdManager.h>

static NSString *kCollectionViewCellIdentifier = @"feedCell";

@interface DevAppFullScreenDemoViewController ()
@property (nonatomic, strong)TPPlayerViewController *playerViewController;
//@property(nonatomic, strong )AdobePassIdentityServicesViewController *identityServicesViewController;
@property(nonatomic, strong)TPIdentityServicesViewController *identityServicesViewController;
@property (nonatomic, strong)TPFeed *feed;
@property(nonatomic, assign)BOOL userIsAuthorized;
@property(nonatomic, strong)DevAppContentItem *contentItem;
@property (nonatomic, assign) NSUInteger selectedItemIndex;
@property(nonatomic, assign)BOOL shouldPlayFullScreenImmediately;
@property(nonatomic, strong)TPFreeWheelManager *adManager;

@end

@implementation DevAppFullScreenDemoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- ( void )awakeFromNib;
{
    self.selectedItemIndex = 999999; // just some ridiculously large number so it will be different from the user's first selection
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.shouldPlayFullScreenImmediately = YES;
    UINib *nib = [ UINib nibWithNibName:NSStringFromClass([ DevAppSimpleCollectionViewCell class]) bundle:[ NSBundle mainBundle]];
    [ self.feedCollectionView registerNib:nib forCellWithReuseIdentifier:kCollectionViewCellIdentifier ];
	// Do any additional setup after loading the view.
    NSString *urlForPlatformFeed = @"http://feed.theplatform.com/f/3JHsmB/KrrrktQbbbHF?form=json";
    TPFeedRequest *feedRequest = [ [ TPFeedRequest alloc ]initWithURL:urlForPlatformFeed delegate:self];
    
    feedRequest.pathName = @"DevAppFeedOneContentList";
    [ feedRequest requestFeed ];
}


- ( void )feedRequest:( TPFeedRequest *)feedRequest isLoaded:( TPFeed *)feed;
{
    self.feed = feed;
    self.dataLoadIsComplete = YES;
    [self.mainActivityIndicator stopAnimating ];
    [ self.feedCollectionView reloadData];
    if (self.feed.contentItems.count) {
        self.selectedItemIndex = 999999;  // some very large number so we can tell it has changed
    }
    DLog(@"feed = %@", feed );
}


- ( void )feedRequest:( TPFeedRequest *)feedRequest failedWithError:( NSError *)error;
{
    DLog(@"feed error = %@", error.userInfo );
    [self loadDataForURLFailed:[error localizedDescription ] ];
    self.dataLoadIsComplete = YES;
}

- ( void )loadDataForURLFailed:( NSString *)errorMessage;
{
    DLog(@"feed load failed");
    
    [self.mainActivityIndicator stopAnimating];
    self.noContentLabel.hidden = NO;
}


#pragma - mark collectionViewDataSource methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.feed.contentItems.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    DevAppSimpleCollectionViewCell *cell = (DevAppSimpleCollectionViewCell *)[ self.feedCollectionView dequeueReusableCellWithReuseIdentifier:kCollectionViewCellIdentifier forIndexPath:indexPath];
    DevAppContentItem *contentItem = self.feed.contentItems[indexPath.row];
    UIImage *thumbnail = [contentItem imageForRole:contentItem.roleForDefaultThumbnail ];
    [ contentItem goGetImageForRole:contentItem.roleForDefaultThumbnail   metadata:cell.imageView delegate:self selector:@selector(thumbnailImageLoaded:) activityIndicator:nil ];
    
    cell.imageView.image = thumbnail;
    cell.buttonLabel.text = contentItem.title;
    return cell;
}

#pragma - mark collectionViewDelegate methods

- ( void )collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath;
{
    //    DevAppSimpleCollectionViewCell *cell = (DevAppSimpleCollectionViewCell *)[ self.feedCollectionView cellForItemAtIndexPath:indexPath];
    DevAppContentItem *selectedItem = [ self.feed.contentItems objectAtIndex:indexPath.row];
    [ self changeToContentItem:selectedItem];
}

#pragma - thumbnail-related methods

- ( void)changeToContentItem:( DevAppContentItem *)tappedContentItem;
{
    self.contentItem = tappedContentItem;
    //    [ self.metadataView populateFromContentItem:self.contentItem ];
    //    [ self handleMetadataViewDisplay:YES ];
    //    self.identityServicesViewController.currentlySelectedVideoAsset = self.contentItem.videoAsset;    
    self.playerViewController = [[ TPPlayerViewController alloc]initWithNibName:nil bundle:nil ];
    self.adManager = [[TPFreeWheelManager alloc] init];
    self.playerViewController.adManager = self.adManager;

    self.playerViewController.isAlwaysFullScreen = YES;
    self.playerViewController.playsFullScreenOnly = YES;
    self.playerViewController.shouldPlayFullScreenImmediately = YES;
    
    // set the class for the custom controls
    self.playerViewController.playerControlClassName = @"TPCustomPlayerControls";
    // set self as the delegate and the interestedParty (this also causes interestedParty to be set on the TPPlayerControls
    self.playerViewController.interestedParty = self;
    self.playerViewController.delegate = self;
    
    
    CGRect frame = self.view.bounds;
    self.playerViewController.view.frame = frame;
    
    self.playerViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [ self presentViewController:self.playerViewController animated:YES completion:^{
        [ self.playerViewController userChangedSelection:self.contentItem];
    } ];
}

- (void)userHitCancel:(id)sender;
{
    [ self.presentedViewController dismissViewControllerAnimated:YES completion:nil ];
}

- ( void )thumbnailImageLoaded:( TPDataHandler *)dataHandler;
{
    NSError *error = dataHandler.error;
    
    if( ! error )
    {
        UIImage *thumbnail = dataHandler.returnedObject;
        UIImageView *imageView = dataHandler.metadata;
        imageView.image = thumbnail;
    }
}

- (void)playerCancelled;  // hit done button or cancel button
{
    [self.playerViewController.presentingViewController dismissViewControllerAnimated:YES completion:^{
        self.playerViewController = nil;
    } ];
}

- (void)playbackDidFinish;
{
    [self.playerViewController.presentingViewController dismissViewControllerAnimated:YES completion:^{
        self.playerViewController = nil;
    } ];
}

#pragma - mark methods for TPCustomPlayerControlsInterestedParty

- ( NSString *)textForVideoLabel;
{
    return self.contentItem.title;
}

@end
