//
//  TPSimpleLoginViewController.h
//  ThePlatformSampleWithFramework
//
//  Copyright © 2014, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)

//

#import <UIKit/UIKit.h>

@interface TPSimpleLoginViewController : UIViewController


@property (weak, nonatomic) IBOutlet UITextField *userNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;

- (IBAction)userPressedLoginButton:(id)sender;

@end
