//
//  DevAppFeedOneViewController.m


#import "DevAppFeedOneViewController.h"
#import "TPSimpleContentItemView.h"

@interface DevAppFeedOneViewController ()

@property (nonatomic, strong)TPPlayerViewController *playerViewController;
@property (nonatomic, strong)TPFeed *feed;
@property (nonatomic, strong)TPMetadataView *metadataView;
@property (nonatomic, assign) NSUInteger selectedItemIndex;

@end

@implementation DevAppFeedOneViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- ( void )awakeFromNib;
{
    self.selectedItemIndex = 999999; // just some ridiculously large number so it will be different from the user's first selection
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.playerViewController = [ [ TPPlayerViewController alloc]initWithNibName:nil bundle:nil ];
    self.playerViewController.fadeDuration = 1.0;
    self.playerViewController.view.frame = self.topView.bounds;
    [ self addChildViewController:self.playerViewController ];
    [self.topView addSubview:self.playerViewController.view ];
    [ self.playerViewController didMoveToParentViewController:self ];
    self.metadataView = [ TPMetadataView metadataView ];    
    CGRect frame = self.topView.bounds;
    frame.origin = CGPointMake(frame.origin.x, CGRectGetMaxY(frame) - self.metadataView.bounds.size.height);
    frame.size.height = self.metadataView.bounds.size.height;
    self.metadataView.frame = frame;
    
    [ self.topView addSubview:self.metadataView];
    self.playerViewController.metadataView = self.metadataView;

    
    NSString *urlForPlatformFeed = @"http://feed.theplatform.com/f/3JHsmB/KrrrktQbbbHF?form=json";
    TPFeedRequest *feedRequest = [ [ TPFeedRequest alloc ]initWithURL:urlForPlatformFeed delegate:self];
    [ feedRequest requestFeed ];

}

- ( void )viewWillAppear:(BOOL)animated;
{
    [ super viewWillAppear:animated];
    if (self.dataLoadIsComplete && self.feed.contentItems.count > self.selectedItemIndex) {
        TPContentItem *tappedContentItem = [ self.feed.contentItems objectAtIndex:self.selectedItemIndex ];
        [ self.playerViewController userChangedSelection:tappedContentItem];
    }
}

- ( void )feedRequest:( TPFeedRequest *)feedRequest isLoaded:( TPFeed *)feed;
{
    self.feed = feed;
    self.dataLoadIsComplete = YES;

    if (self.feed.contentItems.count) {
        [ self populateThumbnails ];
        self.selectedItemIndex = 0;
        TPContentItem *tappedContentItem = [ self.feed.contentItems objectAtIndex:0];
        [ self.playerViewController userChangedSelection:tappedContentItem];
    }
    DLog(@"feed = %@", feed );
}

- ( void )feedRequest:( TPFeedRequest *)feedRequest failedWithError:( NSError *)error;
{
    DLog(@"feed error = %@", error.userInfo );
    [self loadDataForURLFailed:[error localizedDescription ] ];
    self.dataLoadIsComplete = YES;
}

- ( void )populateThumbnails;
{
        CGFloat nextX = 4.0;
        CGFloat y = 10;
        NSUInteger i = 0;
        
        for (TPContentItem *contentItem in self.feed.contentItems ) {
            TPSimpleContentItemView *simpleContentItemView = [TPSimpleContentItemView simpleContentItemView];
            // give it a tag so we can identify it later
            simpleContentItemView.tag = 100 + i++;
//            DLog(@"simpleContentItemView tag = %d", simpleContentItemView.tag);
            // add a gesture recognizer to respond to taps
            UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userTappedContentItem:)];
            [ simpleContentItemView addGestureRecognizer:gestureRecognizer];
            // position it
            CGRect frame = simpleContentItemView.frame;
            frame.origin = CGPointMake(nextX, y);
            simpleContentItemView.frame = frame;
            nextX += simpleContentItemView.frame.size.width + 10.0;
            // add it to the scrollview
            [self.scrollView addSubview:simpleContentItemView ];
            // populate it
            UIImage *thumbnail = [contentItem imageForRole:@"image160x90" ];
            [ contentItem goGetImageForRole:@"image160x90"  metadata:simpleContentItemView delegate:self selector:@selector(thumbnailImageLoaded:) activityIndicator:nil ];

            simpleContentItemView.imageView.image = thumbnail;
            simpleContentItemView.buttonLabel.text = contentItem.title;
        }
        self.scrollView.contentSize = CGSizeMake(nextX, self.scrollView.bounds.size.height);
}

- ( void )thumbnailImageLoaded:( TPDataHandler *)dataHandler;
{
    UIImage *thumbnail = dataHandler.returnedObject;
    TPSimpleContentItemView *contentItemView = (TPSimpleContentItemView *)dataHandler.metadata;
//    NSUInteger tag = [dataHandler.metadata intValue ];
////    DLog(@"tag = %d", tag);
//    if (thumbnail && tag > 99 )
//    {
//        TPSimpleContentItemView *contentItemView = (TPSimpleContentItemView *)[self.scrollView viewWithTag:tag ];
    if (contentItemView) {
        contentItemView.imageView.image = thumbnail;
    }

}

- ( void )userTappedContentItem:( UITapGestureRecognizer *)tapGestureRecognizer;
{
    NSUInteger tagForTappedItem = [ tapGestureRecognizer view].tag;
    NSUInteger indexOfTappedItem = tagForTappedItem - 100;
    if (indexOfTappedItem != self.selectedItemIndex) {
        self.selectedItemIndex = indexOfTappedItem;
        TPContentItem *tappedContentItem = [ self.feed.contentItems objectAtIndex:self.selectedItemIndex];
        [ self.playerViewController userChangedSelection:tappedContentItem];
        DLog(@"tapped item = %@", tappedContentItem);
    }
}

- ( void )loadDataForURLFailed:( NSString *)errorMessage;
{
    DLog(@"feed load failed");
    
    [self.mainActivityIndicator stopAnimating];
    self.noContentLabel.hidden = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setTopView:nil];
    [self setScrollView:nil];
    [self setNoContentLabel:nil];
    [self setMainActivityIndicator:nil];
    [super viewDidUnload];
}

@end
