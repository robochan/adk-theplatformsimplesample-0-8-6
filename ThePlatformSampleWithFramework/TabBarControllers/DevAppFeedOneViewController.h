//
//  DevAppFeedOneViewController.h

#import <UIKit/UIKit.h>
#import "TPPlayer/TPPlayer.h"

@interface DevAppFeedOneViewController : TPNetworkAwareViewController<TPFeedRequestDelegate >

@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *noContentLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *mainActivityIndicator;


@end
