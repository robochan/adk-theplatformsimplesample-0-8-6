//
//  main.m
//  ThePlatformSampleWithFramework
//

#import <UIKit/UIKit.h>

#import "TPAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TPAppDelegate class]));
    }
}
