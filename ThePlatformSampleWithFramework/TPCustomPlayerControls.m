//
//  TPCustomPlayerControls.m
//  ThePlatformSampleWithFramework
//
//  Copyright © 2015, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import "TPCustomPlayerControls.h"
#import <MediaPlayer/MediaPlayer.h>

// add an alternate image and put its name here to substitute for the current adTicks.
#define kTICK_IMAGE_NAME @"tick_mark.png"

@interface TPCustomPlayerControls ()

@property(nonatomic, strong )NSMutableArray *adTicks;

@end

@implementation TPCustomPlayerControls

// TPPlayerControls uses a different designated initializer.  This is what we want to override
-(id)initWithPlayerView:(TPPlayerView *)playerView fullScreen:( BOOL )playerIsInFullScreenMode;
{
    self = [super initWithPlayerView:playerView fullScreen:playerIsInFullScreenMode ];
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    BOOL deviceIsIpad = [ [ UIDevice currentDevice]userInterfaceIdiom] == UIUserInterfaceIdiomPad;
    
    // when the player starts or stops for whatever reason, sync the play/pause button
    [[ NSNotificationCenter defaultCenter]addObserver:self selector:@selector(playerRateChanged:) name:kTPPlayerDidChangeRateNotification object:nil ];
    
    // get the formatted video title from the interested Party
    UIViewController<TPCustomPlayerControlsInterestedParty> *quickPlayVC = (UIViewController<TPCustomPlayerControlsInterestedParty> *)self.interestedParty;
    NSString *textForVideoLabel = [ quickPlayVC textForVideoLabel ];
    self.videoDetailsLabel.text = textForVideoLabel;
    
    // Set custom fonts
    self.videoDetailsLabel.font = deviceIsIpad ? [UIFont fontWithName:@"Gotham-Book" size:18.f] : [UIFont fontWithName:@"Gotham-Book" size:13.f];
    self.statusLabel.font = deviceIsIpad ? [UIFont fontWithName:@"Gotham-Book" size:18.f] : [UIFont fontWithName:@"Gotham-Book" size:13.f];
    self.playheadPositionLabel.font = [UIFont fontWithName:@"Gotham-Book" size:15.f];
    self.durationLabel.font = [UIFont fontWithName:@"Gotham-Book" size:15.f];
    
    
    // this overrides what was set in super
    UIImage *thumbToUse = [UIImage imageNamed:@"btn_player_scrub"];
    [self.playbackSlider setThumbImage:thumbToUse forState:UIControlStateNormal];
    UIImage *highlightThumbToUse = [UIImage imageNamed:@"btn_player_scrub_pressed"];
    [self.playbackSlider setThumbImage:highlightThumbToUse forState:UIControlStateHighlighted];
    
    // Set custom slider backgrounds
    UIImage *sliderBackgroundBase = [[UIImage imageNamed:@"player_progress_base"]
                                     stretchableImageWithLeftCapWidth:2.0 topCapHeight:0.0];
    UIImage *sliderBackgroundActive = [[UIImage imageNamed:@"player_progress_active"]
                                       stretchableImageWithLeftCapWidth:2.0 topCapHeight:0.0];
    
    [self.playbackSlider setMinimumTrackImage:sliderBackgroundActive forState:UIControlStateNormal];
    [self.playbackSlider setMaximumTrackImage:sliderBackgroundBase forState:UIControlStateNormal];
    
    
    UIImage *scrubberBackground = deviceIsIpad ? [UIImage imageNamed:@"scrubber_bkg_blk"] : [UIImage imageNamed:@"scrubber_bkg"];
    scrubberBackground = [scrubberBackground resizableImageWithCapInsets:UIEdgeInsetsMake(0, 33, 0, 33)];
    self.bottomBarImageView.image = scrubberBackground;
    
    self.rewindIncrement = 15;
    
    self.scrubberTimeView.hidden = NO;
}

- ( void )playerRateChanged:( NSNotification *)notification;
{
    DLog(@"playerRateChanged");
    [ self syncPlayPauseButton];
}


- ( void )controlsWereMadeVisible;
{
    DLog(@"controlsWereMadeVisible");
    [ super controlsWereMadeVisible];
    [ self syncPlayPauseButton ];
    CGRect scrubberViewRect = self.scrubberTimeView.frame;
    scrubberViewRect.origin.x = CGRectGetMidX(self.sliderWithAdMarks.horizontalSlider.thumbRect) - ceilf((scrubberViewRect.size.width/2.0)) + self.sliderWithAdMarks.frame.origin.x;
    [UIView animateWithDuration:0.02 delay:0.0 options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         self.scrubberTimeView.frame = scrubberViewRect;
                     }
                     completion:nil];
}

- ( void )rewind:( UIButton *)sender;
{
    [ self.delegate startScrubbing];
    NSTimeInterval sliderValue = self.sliderWithAdMarks.horizontalSlider.value - self.rewindIncrement;
    DLog(@"before self.sliderWithAdMarks.horizontalSlider.value = %g newSlider value %g", self.sliderWithAdMarks.horizontalSlider.value, sliderValue);
    [ self.delegate scrub:sliderValue];
    [ self.delegate stopScrubbing];
    [ self syncScrubberView:sliderValue ];
    DLog(@"after slider value %g",  self.sliderWithAdMarks.horizontalSlider.value);
    
}

- (void)playerTimeDidChange:(NSTimeInterval)timeInSeconds 
{
    [ super playerTimeDidChange:timeInSeconds];
    // we don't need to check for sliding because we pause while scrubbing
    if (self.view.alpha > 0.1)
    {
        [self alignScrubberTimeViewToThumb:self.sliderWithAdMarks.horizontalSlider];
    }
}

- (IBAction)playbackSliderMoved:(TPSlider *)sender
{
    [super playbackSliderMoved:sender];
    [self alignScrubberTimeViewToThumb:sender];
    DLog(@"playbackSliderMoved");
}

- (void)alignScrubberTimeViewToThumb:(TPSlider *)sender
{
    DLog(@"alignScrubberTimeViewToThumb");
    CGRect scrubberViewRect = self.scrubberTimeView.frame;
    scrubberViewRect.origin.x = CGRectGetMidX(sender.thumbRect) - ceilf((scrubberViewRect.size.width/2.0)) + self.sliderWithAdMarks.frame.origin.x;
    
    //    DLog(@"The thumb MidX is %f & ScrubView Origin is %f", CGRectGetMidX(sender.thumbRect), scrubberViewRect.origin.x);
    
    self.scrubberTimeView.frame = scrubberViewRect;
    
}


- (IBAction)aspectButtonTapped:(id)sender
{
    [super userHitAspectButton:sender];
    self.aspectButton.selected = !self.aspectButton.selected;
    
    if (([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)) {
        // Use black background for letterbox so it blends in - iPad ONLY
        UIImage *letterboxScrubberBackground = [UIImage imageNamed:@"scrubber_bkg_blk"];
        letterboxScrubberBackground = [letterboxScrubberBackground resizableImageWithCapInsets:UIEdgeInsetsMake(0, 33, 0, 33)];
        
        // Use gray background when zoomed to set apart - iPad ONLY
        UIImage *zoomedScrubberBackground = [UIImage imageNamed:@"scrubber_bkg"];
        zoomedScrubberBackground = [zoomedScrubberBackground resizableImageWithCapInsets:UIEdgeInsetsMake(0, 33, 0, 33)];
        
        self.bottomBarImageView.image = self.aspectButton.selected ? zoomedScrubberBackground : letterboxScrubberBackground;
    }
}


- ( IBAction)userTappedRewind:(id)sender
{
    DLog(@"User tapped rewind");
    [ self rewind:(UIButton *)sender];
}



- (IBAction)ccButtonSelected:(id)sender {
    [ super ccButtonSelected:sender];
    if (self.ccButton.selected) {
        [ [TPAnalyticsCommander sharedInstance ] trackVideoView:self.interestedParty stage:@"video closed caption on" ];
    }
    else{
        [ [TPAnalyticsCommander sharedInstance ] trackVideoView:self.interestedParty stage:@"video closed caption off" ];
    }
}


-(IBAction)handlePlayAndPauseButton:(id)sender {
    DLog(@"handlePlayAndPauseButton");
    [ super handlePlayAndPauseButton:sender];
    
    if ([(UIButton *)sender isSelected] == NO) {
        [ [TPAnalyticsCommander sharedInstance ] trackVideoView:self.interestedParty stage:@"video resume" ];
    }
}

- ( NSString *)tickMarkImageName;
{
    return TICK_IMAGE_NAME;
}


@end
