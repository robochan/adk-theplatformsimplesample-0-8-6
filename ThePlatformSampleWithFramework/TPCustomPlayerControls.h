//
//  TPCustomPlayerControls.h
//  ThePlatformSampleWithFramework
//
//  Copyright © 2015, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)

//

#import <TPPlayer/TPPlayer.h>

@protocol TPCustomPlayerControlsInterestedParty

- ( NSString *)textForVideoLabel;

@end



@interface TPCustomPlayerControls : TPPlayerControls

@property(nonatomic, weak)IBOutlet UILabel *videoDetailsLabel;
@property(nonatomic, weak)IBOutlet UIImageView *logoImageView;
@property(nonatomic, assign)NSUInteger rewindIncrement;
@property (weak, nonatomic) IBOutlet UIButton *rewindButton;

@property (weak, nonatomic) IBOutlet UIView *scrubberTimeView;
- ( IBAction )aspectButtonTapped:(id)sender;
- ( IBAction)userTappedRewind:(id)sender;



@end
