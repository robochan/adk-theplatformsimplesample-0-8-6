//
//  TPSimpleContentItemView.m
//  ThePlatformSampleWithFramework
//


#import "TPSimpleContentItemView.h"

@implementation TPSimpleContentItemView

+ ( TPSimpleContentItemView *)simpleContentItemView;
{
    NSString *nibName = @"TPSimpleContentItemView";
    UINib *nib = [ UINib nibWithNibName:nibName bundle:nil ];
    NSArray *nibObjects = [ nib instantiateWithOwner:nil options:nil ];
    NSAssert2( [ nibObjects count ] > 0 && [ [ nibObjects objectAtIndex:0 ] isKindOfClass:[ self class ] ], @"Nib %@ does not contain top level view of type %@", nibName, nibName );
    return [ nibObjects objectAtIndex:0 ];
}

@end
