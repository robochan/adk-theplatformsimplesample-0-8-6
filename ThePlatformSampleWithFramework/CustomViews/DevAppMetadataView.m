//
//  DevAppMetadataView.m
//  ThePlatformSampleWithFramework
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import "DevAppMetadataView.h"

@implementation DevAppMetadataView

+ ( DevAppMetadataView *)metadataView;
{
    NSString *nibName = @"DevAppMetadataView";
    UINib *nib = [ UINib nibWithNibName:nibName bundle:nil ];
    NSArray *nibObjects = [ nib instantiateWithOwner:nil options:nil ];
    NSAssert2( [ nibObjects count ] > 0 && [ [ nibObjects objectAtIndex:0 ] isKindOfClass:[ self class ] ], @"Nib %@ does not contain top level view of type %@", nibName, nibName );
    return [ nibObjects objectAtIndex:0 ];
}

- ( void )populateFromContentItem:( DevAppContentItem *)contentItem;
{
    [ super populateFromContentItem:contentItem];
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
//    
//    if (contentItem.expirationDate == 0) {
//        self.availabilityValue.text = NSLocalizedString(@"NA", @"If there is no content expiration date available");
//    }
//    else
//    {
//        self.availabilityValue.text = [dateFormatter stringFromDate:contentItem.expirationDate];
//    }
//    
//    if (contentItem.availableDate == 0) {
//        self.availableFromValue.text = NSLocalizedString(@"NA", @"If there is no content available date available");
//    }
//    else
//    {        
//        self.availableFromValue.text = [dateFormatter stringFromDate:contentItem.availableDate];
//    }
//    
//    NSString *runtimeFormattedString = [self timeFormatted:(int)contentItem.videoAsset.duration ];
//    self.runtimeValue.text = runtimeFormattedString;
//    
//    if (contentItem.synopsis.length) {
//        self.synopsis.text = contentItem.synopsis;
//    }
//    else{
//        self.synopsis.text = contentItem.textDescription;
//    }
    //    [self.synopsis setVerticalAlignmentTop];
    self.episodeTitle.text = contentItem.title;
    

    NSString *localSeasonWord = NSLocalizedString(@"Season", @"Season for a show - multi-use" );
    NSString *localEpisodeWord = NSLocalizedString(@"Episode", @"Episode of a show in a season - multi-use" );
    self.seasonEpisodeLabel.text = [NSString stringWithFormat:@"%@ %d, %@ %d",localSeasonWord, contentItem.season, localEpisodeWord, contentItem.episode ];
    
}

//- (NSString *)timeFormatted:(int)totalSeconds
//{
//    
//    int seconds = totalSeconds % 60;
//    int minutes = (totalSeconds / 60) % 60;
//    int hours = totalSeconds / 3600;
//    
//    return [NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds];
//}

@end
