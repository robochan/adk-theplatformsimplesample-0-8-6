//
//  DevAppSimpleCollectionViewCell.h
//  ThePlatformSampleWithFramework
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)

#import <UIKit/UIKit.h>

@interface DevAppSimpleCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *buttonLabel;
@property (weak, nonatomic) IBOutlet UIImageView *lockImageView;

@end
