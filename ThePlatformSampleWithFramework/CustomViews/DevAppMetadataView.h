//
//  DevAppMetadataView.h
//  ThePlatformSampleWithFramework
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import <UIKit/UIKit.h>
#import "TPPlayer/TPPlayer.h"
#import "DevAppContentItem.h"

@interface DevAppMetadataView : TPMetadataView

@property (weak, nonatomic) IBOutlet UILabel *seasonEpisodeLabel;
@property (weak, nonatomic) IBOutlet UILabel *availabilityLabel;
@property (weak, nonatomic) IBOutlet UILabel *runtimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *availableFromLabel;
@property (weak, nonatomic) IBOutlet UILabel *availabilityValue;
@property (weak, nonatomic) IBOutlet UILabel *runtimeValue;
@property (weak, nonatomic) IBOutlet UILabel *availableFromValue;
@property (weak, nonatomic) IBOutlet UILabel *synopsis;
@property (weak, nonatomic) IBOutlet UILabel *episodeTitle;

+ ( DevAppMetadataView *)metadataView;

- ( void )populateFromContentItem:( DevAppContentItem *)contentItem;

@end
