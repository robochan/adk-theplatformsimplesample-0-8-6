//
//  TPSimpleContentItemView.h
//  ThePlatformSampleWithFramework
//


#import <UIKit/UIKit.h>

@interface TPSimpleContentItemView : UIView

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *buttonLabel;
@property (weak, nonatomic) IBOutlet UIImageView *lockImageView;


+ ( TPSimpleContentItemView *)simpleContentItemView;

@end
