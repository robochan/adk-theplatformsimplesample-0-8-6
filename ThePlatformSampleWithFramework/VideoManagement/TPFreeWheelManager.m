//
//  TPFreeWheelManager.m

#import "TPFreeWheelManager.h"

@interface TPFreeWheelManager()

// this is weak -- it is just a convenience method to reference the videoManager.player property
@property (nonatomic, retain) NSMutableArray *temporalSlots;
@property(nonatomic, strong) NSMutableArray *overlaySlots;
@property(nonatomic, strong) NSMutableArray *midrollSlots;

@property(nonatomic, strong) NSArray *overlayAdTimes;
@property(nonatomic, strong) NSTimer *adTimer;
@property(nonatomic, assign)CMTime mCurrentAdTime;

@property(nonatomic, assign)CGFloat periodicTimeout;

@property (nonatomic, retain) id<FWContext> adContext;

@end

@implementation TPFreeWheelManager

- (void)submitAdRequest:( NSString *)adManagerContentId videoManager: (UIViewController<TPAdManagerOwner> *)videoManager;
{
    DLog( );
    [ super submitAdRequest:adManagerContentId videoManager:videoManager];
    TPAppSpecificInformationManager *appSpecificInformationManager = [ TPAppSpecificInformationManager sharedInstance];
    
    self.periodicTimeout = [[ appSpecificInformationManager objectForKey:@"FreeWheelPeriodicTimeout"]floatValue ];
//    FWSetLogLevel( FW_LOG_LEVEL_QUIET);
    
	//set current viewController object to FreeWheel to ensure FreeWheel can correctly handle the FullScreen video ad play.
    // it doesn't seem to matter if we set currentViewController to self or to self.videoManager --
	[ adManager setCurrentViewController:self.videoManager ];
	//create new adContext for each playback; the adContext object is used to gather info for ad request, send ad request to ad server,
	//parse ad info from ad response and take controll of all ads play
	self.adContext = [adManager newContext];
	
    // Freewheel demo values
    //	//contact FreeWheel Integration Support Engineer about configuration of profiles/slots/keyvalue for your integration.
//   NSString *assetID = @"1449645";
    NSString *assetID = adManagerContentId;
//    [ self.adContext setProfile:FW_PROFILE :nil :nil :nil];
//	[ self.adContext setSiteSection:FW_SiteSectionID :0 :0 :FW_ID_TYPE_CUSTOM :0];		
//	[ self.adContext setVideoAsset:assetID :0 :nil :true :0 :0 :FW_ID_TYPE_CUSTOM :0 :FW_VIDEO_ASSET_DURATION_TYPE_EXACT];

    BOOL UseProductionAds = [ appSpecificInformationManager booleanForKey:@"FreeWheelProductionAds"];    
    if (UseProductionAds) {
        NSString *FWProfile = [ appSpecificInformationManager objectForKey:@"FreeWheelProfile"];
        NSString *FWSiteSectionID = [ appSpecificInformationManager objectForKey:@"FreeWheelSiteSectionID"];

        [ self.adContext setPlayerProfile:FWProfile defaultTemporalSlotProfile:nil defaultVideoPlayerSlotProfile:nil defaultSiteSectionSlotProfile:nil];
        [ self.adContext setSiteSectionId:FWSiteSectionID idType:FW_ID_TYPE_CUSTOM pageViewRandom:0 networkId:0 fallbackId:0 ];
    }
    else{
        NSString *FWProfile = [ appSpecificInformationManager objectForKey:@"FreeWheelTestProfile"];
        NSString *FWTestSiteSectionID = [ appSpecificInformationManager objectForKey:@"FreeWheelTestSiteSectionID"];
        [ self.adContext setPlayerProfile:FWProfile defaultTemporalSlotProfile:nil defaultVideoPlayerSlotProfile:nil defaultSiteSectionSlotProfile:nil];
        [ self.adContext setSiteSectionId:FWTestSiteSectionID idType:FW_ID_TYPE_CUSTOM pageViewRandom:0 networkId:0 fallbackId:0 ];
    }
//    [ self.adContext setPlayerProfile:FW_TEST_PROFILE defaultTemporalSlotProfile:nil defaultVideoPlayerSlotProfile:nil defaultSiteSectionSlotProfile:nil];
//    [ self.adContext setSiteSectionId:FW_TEST_SiteSectionID idType:FW_ID_TYPE_CUSTOM pageViewRandom:0 networkId:0 fallbackId:0 ];
    [ self.adContext setVideoAssetId:assetID idType:FW_ID_TYPE_CUSTOM duration:0 durationType:FW_VIDEO_ASSET_DURATION_TYPE_EXACT location:nil autoPlayType:FW_VIDEO_ASSET_AUTO_PLAY_TYPE_ATTENDED videoPlayRandom:0 networkId:0 fallbackId:0];
    
    [ self positionOverlay ];    
	//tell the ad context on which base view object to render video ads
    [self.adContext setVideoDisplayBase:self.videoManager.view ];
    
	//register callback handler for ad request complete event
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAdRequestCompleted:) name:FW_NOTIFICATION_REQUEST_COMPLETE object:self.adContext];
	
	//submit ad request to the Freewheel ad server
    NSNumber *adRequestTimeout = [ appSpecificInformationManager objectForKey:@"FreeWheelAdRequestTimeout"];

	[self.adContext submitRequestWithTimeout:[adRequestTimeout floatValue] ];	
}

- ( void )displayBaseChanged;
{
    [self.adContext setVideoDisplayBase:self.videoManager.view ];
}

- (void)onAdRequestCompleted:(NSNotification *)notification
{
    DLog( );

    // prep the pre-roll ad slots
//    NSArray *preRollSlots = self.shouldSkipPreRoll ? @[] : [self.adContext getSlotsByTimePositionClass:FW_TIME_POSITION_CLASS_PREROLL];
//    self.temporalSlots  = [ NSMutableArray arrayWithArray:preRollSlots];
    self.temporalSlots  = [ NSMutableArray arrayWithArray:[self.adContext getSlotsByTimePositionClass:FW_TIME_POSITION_CLASS_PREROLL]];
	for (id<FWSlot> slot in self.temporalSlots) {
		[slot preload];
	}
    [ self setupMidrollSlots];
    [ self setupOverlaySlots];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAdSlotEnded:) name:FW_NOTIFICATION_SLOT_ENDED object:self.adContext ];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pauseContentVideo) name:FW_NOTIFICATION_CONTENT_PAUSE_REQUEST object:self.adContext];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resumeContentVideo) name:FW_NOTIFICATION_CONTENT_RESUME_REQUEST object:self.adContext];

    [ self playPrerollAdSlots ];
}

- (void)pauseContentVideo {
	if (self.player.playbackState == MPMoviePlaybackStatePlaying) {
		[self unregisterForNotifications];
		[self.player pause];
		[self.adContext setVideoState:FW_VIDEO_STATE_PAUSED];
	}
}

- (void)resumeContentVideo {
	if (self.player.playbackState == MPMoviePlaybackStatePaused) {
		[self registerForNotifications];
		[self.player play];
		[self.adContext setVideoState:FW_VIDEO_STATE_PLAYING];
	}
}

- ( void )setupMidrollSlots;
{
    self.midrollSlots = [ NSMutableArray arrayWithArray:[ self.adContext getSlotsByTimePositionClass:FW_TIME_POSITION_CLASS_MIDROLL ]];
    DLog(@"*** midrollSlots = %@", self.midrollSlots);
    self.midrollAdTimes  = [self timesArrayFromSlots:self.midrollSlots ];
}

- (void )setupOverlaySlots;
{
    self.overlaySlots = [ NSMutableArray arrayWithArray:[ self.adContext getSlotsByTimePositionClass:FW_TIME_POSITION_CLASS_OVERLAY] ];
    DLog(@"*** overlaySlots = %@", self.overlaySlots);
    self.overlayAdTimes  = [self timesArrayFromSlots:self.overlaySlots ];
}


- (NSArray *)timesArrayFromSlots:( NSArray *)slots
{
    NSMutableArray * timesToUse = [NSMutableArray array];
    
    for (id<FWSlot> iter in slots)
    {
        NSTimeInterval timePosition = [iter timePosition];
        CMTime curTimeCMTime = CMTimeMake((int)timePosition * 600, 600);
                
        NSValue * theValue = [NSValue valueWithCMTime:curTimeCMTime];
        CGFloat timeToShow = CMTimeGetSeconds(curTimeCMTime) / 60.0;
        DLog(@"timeValue for slot = %@  %g", theValue, timeToShow);
        [timesToUse addObject:theValue];
    }
    return timesToUse;
}

- (void)playPrerollAdSlots
{
    DLog( );
	//play the preroll slot returned by the ad server.
    //because there maybe multiple prerolls, app needs to play them sequentially 
    if (self.shouldSkipPreRoll) {
        [ self playContentMovie ];
    }
    else{
        [ self playNextPreroll];
    }
}

- (void)playNextPreroll
{
    DLog( @"***  playNextPreroll");
	if (self.temporalSlots.count > 0) {
		id<FWSlot> slot = [self.temporalSlots objectAtIndex:0];
        DLog( @"***  playing preroll slot");
		[self.temporalSlots removeObjectAtIndex:0];
		[slot play];
        DLog(@"our slot play");
	} else {
		//if there is no preroll slot, setUp midroll then start the content video
        DLog(@"*** no more pre-roll playContentMovie ");
        [ self playContentMovie ];
	}
}

- (void)playNextPostroll {
    DLog( @"***  playNextPostroll");
	if ( self.temporalSlots.count > 0) 
    {
        DLog(@"*** playing Postroll slot ");
		id<FWSlot> slot = [ self.temporalSlots objectAtIndex:0 ];
		[ self.temporalSlots removeObjectAtIndex:0 ];
		[ slot play ];
        DLog(@"our slot play");
	} else {
		//if there is no postroll slot, clean up
        DLog(@"*** no more post-roll cleanup ");
       [ self.videoManager stopShowingFullscreen];
		[ self cleanup];
	}
}

- (void)cleanup
{
    DLog( );
	//tell adManager to release the ViewController
	[ adManager setCurrentViewController:nil];
//	self.adContext = nil;
    [ super cleanup ];
}

- (void)playPostrollAdSlots {
	[ self.temporalSlots removeAllObjects];
	[ self.temporalSlots addObjectsFromArray:[ self.adContext getSlotsByTimePositionClass:FW_TIME_POSITION_CLASS_POSTROLL]];
    DLog(@"*** playPostrollAdSlots  player is fullscreen %@", YES_OR_NO(self.player.fullscreen));
//	[ self.adContext setMoviePlayerFullscreen:self.player.fullscreen];
    [ self playNextPostroll];
}

- (void)playOverlayAdSlot:( NSValue *)slotTime
{
    NSMutableArray *slotsToRemove = [NSMutableArray array ];

    CMTime adTime = [ slotTime CMTimeValue];
    DLog(@"*** playOverlayAdSlot %@", slotTime );
    
    double timeSlot = CMTimeGetSeconds(adTime);
	for (id<FWSlot> slot in self.overlaySlots)
    {
		if (abs([slot timePosition] - timeSlot) < 2)
        {
            DLog(@"[slot timePosition] - timeSlot) < 2" );
            [ slotsToRemove addObject:slot ];
			[ slot play ];
            DLog(@"our slot play");
			break;    
		}
	}
    [ self.overlaySlots removeObjectsInArray:slotsToRemove];
}

- (void)playMidrollAdSlot:( NSValue *)slotTime
{
    NSMutableArray *slotsToRemove = [NSMutableArray array ];
    DLog(@"*** playMidrollAdSlot %@", slotTime );
    CMTime adTime = [ slotTime CMTimeValue];
    double timeSlot = CMTimeGetSeconds(adTime);
    
	for (id<FWSlot> slot in self.midrollSlots)
    {
		if (abs([slot timePosition] - timeSlot) < 2)
        {
            DLog(@"*** playing MidrollAdSlot %@", slotTime );
            self.isPlayingMidroll = YES;
            [ slotsToRemove addObject:slot ];
			[ slot play];
            DLog(@"our slot play timePosition = %f", [ slot timePosition]);
			break;    
		}
	}
    [ self.midrollSlots removeObjectsInArray:slotsToRemove];
}

- (void)playContentMovie;
{
    DLog( );
    
    [self registerForNotifications];
    [ self.videoManager playContentMovie ];
    [ self startTimer ];
}

- ( void)startTimer;
{
    self.adTimer = [NSTimer scheduledTimerWithTimeInterval:self.periodicTimeout target:self selector:@selector(doPeriodicTasks:) userInfo:nil repeats:YES];
}

//all FreeWheel slots will send SlotEnded notification when it ends
- (void)onAdSlotEnded:(NSNotification *)notification {
	if (FW_TIME_POSITION_CLASS_PREROLL == [[self.adContext getSlotByCustomId:[[notification userInfo] objectForKey:FW_INFO_KEY_CUSTOM_ID]] timePositionClass]) {
        // When the preroll slot ends, play content video.
        DLog(@"*** playNextPreroll");
		[self playNextPreroll];
	} else if (FW_TIME_POSITION_CLASS_POSTROLL == [[self.adContext getSlotByCustomId:[[notification userInfo] objectForKey:FW_INFO_KEY_CUSTOM_ID]] timePositionClass]) {
        DLog(@"*** playNextPostroll");
        [self playNextPostroll];
	}
    else if (FW_TIME_POSITION_CLASS_MIDROLL == [[self.adContext getSlotByCustomId:[[notification userInfo] objectForKey:FW_INFO_KEY_CUSTOM_ID]] timePositionClass]) {
        DLog(@"*** continueContentMovie");
//        [self continueContentMovie ];
    }
    else if (FW_TIME_POSITION_CLASS_OVERLAY == [[self.adContext getSlotByCustomId:[[notification userInfo] objectForKey:FW_INFO_KEY_CUSTOM_ID]] timePositionClass]) {
        DLog(@"*** overlay ended" );
    }
}

- ( void)continueContentMovie;
{
    DLog(@"*** continueContentMovie" );
    self.isPlayingMidroll = NO;
    [ self registerForNotifications];
    [ self.player play ];
}

- ( void )registerForNotifications;
{
    [ [ NSNotificationCenter defaultCenter ]addObserver:self selector:@selector(loadStateChanged:) name:MPMoviePlayerLoadStateDidChangeNotification object:self.player ];
    [ [ NSNotificationCenter defaultCenter ]addObserver:self selector:@selector(playbackFinished:) name:MPMoviePlayerPlaybackDidFinishNotification object:self.player ];
    [ [ NSNotificationCenter defaultCenter ]addObserver:self selector:@selector(playbackStateChanged:) name:MPMoviePlayerPlaybackStateDidChangeNotification object:self.player ];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willEnterFullScreenVideo:) name:MPMoviePlayerWillEnterFullscreenNotification object:self.player];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willExitFullScreenVideo:) name:MPMoviePlayerWillExitFullscreenNotification object:self.player];
}

- ( void )unregisterForNotifications;
{
	[[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerLoadStateDidChangeNotification object:self.player];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackStateDidChangeNotification object:self.player];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:self.player];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerWillEnterFullscreenNotification object:self.player];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerWillExitFullscreenNotification object:self.player];
}

- (void)loadStateChanged:(NSNotification *)notification {
	NSLog(@"loadStateChanged %@ %d", notification, [self.player loadState]);
	if ( [ self.player loadState] & MPMovieLoadStatePlayable) {
		//this notification might be triggered several times. only need to handle the first playable notification
		[[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerLoadStateDidChangeNotification object:self.player];
		[self.adContext setVideoState:FW_VIDEO_STATE_PLAYING];
	}
}

- (void)playbackFinished:(NSNotification *)notification {
	NSLog(@"playbackFinished %@", notification);	
	[[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:self.player];
	[ self.adContext setVideoState:FW_VIDEO_STATE_COMPLETED];
    [ self.adTimer invalidate ];
    self.adTimer = nil;
	[ self playPostrollAdSlots ];
}

- (void)playbackStateChanged:(NSNotification *)notification {
	NSLog(@"playbackStateChanged %d", self.player.playbackState);
	if ( self.player.playbackState == MPMoviePlaybackStatePlaying)
    {
		[ self.adContext setVideoState:FW_VIDEO_STATE_PLAYING];
	} 
    else if (self.player.playbackState == MPMoviePlaybackStatePaused) 
    {
		[ self.adContext setVideoState:FW_VIDEO_STATE_PAUSED];
	}
}

- (void)willEnterFullScreenVideo:(NSNotification *)notification {
	NSLog(@"willEnterFullScreenVideo %@", notification);
	//inform AdManager that the content played in fullscreen mode, thus next video ad will be played in fullscreen also
//	[ self.adContext setMoviePlayerFullscreen:YES ];
}

- (void)willExitFullScreenVideo:(NSNotification *)notification {
	NSLog(@"willExitFullScreenVideo %@", notification);
	[ self.adContext setVideoDisplayBase:self.videoManager.view ];
}

- ( void )doPeriodicTasks:( NSTimer *)timer;
{
    CMTime currentPlayTime = CMTimeMakeWithSeconds(self.player.currentPlaybackTime, NSEC_PER_SEC);
//    NSInteger elapsedPlayTime = CMTimeGetSeconds(CMTimeSubtract(currentPlayTime, self.previousPlayTime));
    DLog(@"currentPlayTime = %f", self.player.currentPlaybackTime);
//    BOOL userScrubbed = elapsedPlayTime > 2;
//    DLog(@"elapsedPlayTime = %d userScrubbed = %@", elapsedPlayTime, YES_OR_NO(userScrubbed));
//
//    self.previousPlayTime = currentPlayTime;

    if (self.userScrubbed && self.player.currentPlaybackTime > 0) {
        [ self playLastMidrollScrubPassed];
        self.userScrubbed = NO;
    }
    else{
        for (NSValue *cmTimeAsValue in self.midrollAdTimes)
        {
            BOOL shouldPlayAd = [ self timeIsRight:cmTimeAsValue currentPlayTime:currentPlayTime];
            if (shouldPlayAd) {
                DLog(@"*** should Play midroll Ad");
                self.mCurrentAdTime = currentPlayTime;
                [ self playMidrollAdSlot:cmTimeAsValue ];
            }
            else{
            }
        }
    }

    for (NSValue *cmTimeAsValue in self.overlayAdTimes)
    {
        BOOL shouldPlayAd = [ self timeIsRight:cmTimeAsValue currentPlayTime:currentPlayTime];
        if (shouldPlayAd) {
            DLog(@"*** should Play overlay Ad");
            self.mCurrentAdTime = currentPlayTime;
            [ self playOverlayAdSlot:cmTimeAsValue ];
        }
        else{
        }
    }
}

- ( void )playLastMidrollScrubPassed;
{
    CGFloat playTime  = [self.endScrubValue floatValue ];
    CMTime currentPlayTime = CMTimeMake(playTime * 600, 600);
    DLog(@"currentPlayTime for endScrubValue = %f currentPlayTime = %f", playTime, CMTimeGetSeconds(currentPlayTime));
    
    NSValue *timeToPlay = nil;
    for (NSValue *cmTimeAsValue in self.midrollAdTimes)
    {
        CMTime adTime = [ cmTimeAsValue CMTimeValue];
        DLog(@"adTime = %@ currentPlayTime = %f", cmTimeAsValue, CMTimeGetSeconds(currentPlayTime));
        
        if (1 == CMTimeCompare(currentPlayTime, adTime)) {
            timeToPlay = cmTimeAsValue;
         DLog(@"timeToPlay = %@ ", timeToPlay);       }
    }
    if (timeToPlay) {
        self.mCurrentAdTime = currentPlayTime;
        [ self playMidrollAdSlot:timeToPlay];
    }
}




- ( BOOL )timeIsRight:( NSValue  *)cmTimeAsValue currentPlayTime:( CMTime)currentPlayTime;
{
    CMTime adTime = [ cmTimeAsValue CMTimeValue];
    CMTime diffTime = CMTimeAbsoluteValue( CMTimeSubtract(currentPlayTime, adTime) );
    CMTime epsilon  = CMTimeMake ( self.periodicTimeout * 600, 600);
    
    // this is largely from Cypress
    // If we are +/- within our epsilon of an ad start time, notify to start the ad.
    if ( CMTimeCompare(diffTime, epsilon) != 1 )    // -1 == less than, 0 == equal, 1 == greater than. We want less than or equal.
    {
        DLog(@"checkAdTimes    - close to Ad time  currentPlayTime: %f   %f", self.player.currentPlaybackTime, CMTimeGetSeconds(currentPlayTime));
        DLog(@"                -                   diffTime: %f   epsilon: %f", CMTimeGetSeconds(diffTime), CMTimeGetSeconds(epsilon));
        
        return YES;

//        // We need to avoid retriggering the same ad if this method is called too quickly after its start.
//        CMTime currentAdDiffTime = CMTimeAbsoluteValue( CMTimeSubtract(currentPlayTime, self.mCurrentAdTime) );
//        CMTime currentAdEpsilon  = CMTimeMake ( 2.0 * kPeriodicTimerInterval * 600, 600);
//        
//        DLog(@"                - **                AdDiffTime: %f   AdEpsilon: %f", CMTimeGetSeconds(currentAdDiffTime), CMTimeGetSeconds(currentAdEpsilon));
//        
//        if ( CMTimeCompare(currentAdDiffTime, currentAdEpsilon) == 1 )
//        {
//            DLog(@"should play ad");
//            return YES;
//        }
//        else
//        {
//            DLog(@"don't play ad");
//        }
    }
    return NO;
}

- ( void)stopTiming;
{
    [ self.adTimer invalidate];
    self.adTimer = nil;
}

- ( void )closedCaptionsAreNowShowing:( BOOL )closedCaptionsAreShowing;
{
    self.shouldPositionForClosedCaptions = closedCaptionsAreShowing;
    [ self positionOverlay];
}

- ( void )positionOverlay;
{
    NSString *positionToUse = self.shouldPositionForClosedCaptions ? @"tc" : @"bc" ;
    [  self.adContext setParameter:@"renderer.html.primaryAnchor" withValue:positionToUse forLevel:FW_PARAMETER_LEVEL_GLOBAL];
    DLog(@"position To Use = %@", positionToUse);
}



@end
