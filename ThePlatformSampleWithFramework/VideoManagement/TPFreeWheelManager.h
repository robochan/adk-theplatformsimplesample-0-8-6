//
//  TPFreeWheelManager.h

//
//  Compatibility: iOS 5.1+
//
//  SUMMARY
//  The principle configuration and interface class to FW. 
//
//  NOTE: Be certain to use correct values for your application in the FW_PROFILE and FW_TEST_PROFILE.
//  Also be sure that you are correctly overriding the default FW_SiteSectionID in your views.
//
//  DOCUMENTATION Notes (Remove when docs are complete)
//  HN - Let's discuss with FW getting all the valid values for the primary methods and adding them to our docs
//       as they are not in their docs.
//
//  DEVELOPMENT Notes
//  - Add SiteSectionID override method, per request

#import <UIKit/UIKit.h>
#import <MediaPlayer/MPMoviePlayerController.h>
#import <TPPlayer/TPPlayer.h>
#import "FWPlayerCommon.h"


@interface TPFreeWheelManager : TPAdManager

@property (nonatomic, strong)NSString *adSiteId;

@end
