//
//  TPFreewheelAdObject.h
//  TPFreeWheelAdManager
//
//  Copyright © 2014, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)

//

#import <TPPlayer/TPPlayer.h>
#import "FWPlayerCommon.h"

@interface TPFreewheelAdObject : TPAdObject

@property(nonatomic, assign)BOOL played;
@property(nonatomic, assign) NSUInteger adIDAsInteger;
@property(nonatomic, strong)NSString *advertiserName;
@property(nonatomic, strong)NSString *campaignName;
@property(nonatomic, strong)NSString *creativeName;
@property(nonatomic, strong)NSString *adUnitName;
@property(nonatomic, assign)NSUInteger podOrderInPodArray;

- (void)populateFromAdInstance:(id<FWAdInstance>)adInstance withPodID:( NSString *)podID adType:(TPAdType )adType podPosition:( NSUInteger)podPosition orderInPodArray:( NSUInteger)orderInPodArray;

@end