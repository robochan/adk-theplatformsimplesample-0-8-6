//
// FreeWheel AdManager Sample Player
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import "AdManager/FWSDK.h"


#define FWPLAYER_AD_REQUEST_TIMEOUT 5
#define XRETAIN // internal macro
#define XASSIGN // internal macro

// NOTE: Typically, one app has one adManager object
id<FWAdManager> adManager;
