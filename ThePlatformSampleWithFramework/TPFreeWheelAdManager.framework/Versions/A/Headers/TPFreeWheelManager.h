//
//  TPFreeWheelManager.h
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//
//
//  Compatibility: iOS 5.1+
//
//  SUMMARY
//  The principle configuration and interface class to FW. 
//
//  NOTE: Be certain to put correct values for your application in the FreeWheel items in the AppSpecificInformation.plist file
//  This version adds 2 additional values FreeWheelDefaultCompanionAdPosition and FreeWheelCcCompanionAdPosition.  If you don't provide values for these keys,
//  FreeWheelDefaultCompanionAdPosition is 'bc' and FreeWheelCcCompanionAdPosition is 'tc'.  Possible values are:
// 'tc', 'bc', 'ml', 'mr', 'tl', 'tr', 'bl', 'br'  where 't' means 'top', 'l' means 'left', 'b' means 'bottom', 'r' means 'right', 'c' means 'center', 'm' means 'middle'

#import <UIKit/UIKit.h>

#import <TPPlayer/TPPlayer.h>
#import "TPFreewheelAdObject.h"


//@class TPAdManager;

@interface TPFreeWheelManager : TPAdManager

@property (nonatomic, strong)NSString *adSiteId;
@property (nonatomic, assign)BOOL shouldAlsoSendNonPodNotifications;

// the value for each key can either be an array of NSStrings or a single NSString
// if additionalValues is nil, nothing is passed, but if it contains values, they are sent to the FWContext
// through its addValue:forKey: method prior to submiting  the adRequest
@property (nonatomic, strong)NSDictionary *additionalValues;  
// if additionalParameters is nil, nothing is passed, but if it contains values, they are sent to the FWContext
// through its setParameter:withValue:forLevel method (using level FW_PARAMETER_LEVEL_OVERRIDE) prior to submiting  the adRequest
@property (nonatomic, strong)NSDictionary *additionalParameters;

// When the user hits a button in YOUR UI to go to the sponsor's website 
// (and you have included FW_PARAMETER_CLICK_DETECTION : @"false" in the additionalParameters dictionary
// call this method to tell the adManager to bring up the in-app webView.
- ( void )userHitSponsorWebsiteButton;

// To discover if you should show your Visit Sponsor Website button ( because the ad does have a click-through URL)
// or not ( because the ad does NOT have a click-through URL) send currentlyPlayingAdHasClickThrough when you receive the kTPAdSlotStartedNotification notification.
- ( BOOL )currentlyPlayingAdHasClickThrough;

@end
