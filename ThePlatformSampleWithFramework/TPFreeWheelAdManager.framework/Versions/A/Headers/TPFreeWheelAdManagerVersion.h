//
//  TPFreeWheelAdManagerVersion.h
//
//  Copyright © 2013, thePlatform for Media, Inc. All rights reserved. (unless otherwise indicated)
//
//  Compatible with Freewheel iOS SDK 5.19.3

#define TPFREE_WHEEL_AD_MANAGER_VERSION @"0.8.5"
#define TPFREE_WHEEL_AD_MANAGER_BUILD_NUMBER @"82"
